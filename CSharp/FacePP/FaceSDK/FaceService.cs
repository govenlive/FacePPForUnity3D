﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiniJSON;

namespace FaceppSDK
{
    public class FaceService:IFaceService
    {
        private string app_key;
        private string app_secret;

        public string App_key { get { return app_key; } }
        public string App_secret { get { return app_secret; } }
        public FaceService(string _app_key, string _app_secret)
        {
            app_key = _app_key;
            app_secret = _app_secret;
        }
        /// <summary>
        /// 创建FaceService服务类
        /// </summary>
        public FaceService()
        {
        }




        #region detection
        /// <summary>
        /// 同步检测给定图片(Image)中的所有人脸(Face)的位置和相应的面部属性
        /// 目前面部属性包括性别(gender), 年龄(age)和种族(race)
        /// 若结果的face_id没有被加入任何faceset/person之中，则在72小时之后过期被自动清除
        /// </summary>
        /// <param name="url">待检测图片的URL</param>
        /// <param name="mode">检测模式可以是normal(默认) 或者 oneface 。在oneface模式中，检测器仅找出图片中最大的一张脸。</param>
        /// <param name="attribute">可以是 all(默认) 或者 none或者由逗号分割的属性列表。目前支持的属性包括：gender, age, race</param>
        /// <param name="tag">可以为图片中检测出的每一张Face指定一个不超过255字节的字符串作为tag，tag信息可以通过 /info/get_face 查询</param>
        /// <returns>返回一个DetectResult，详细内容请参考FaceEntity.cs代码</returns>
        public DetectResult Detection_Detect(string url, string mode = "", string attribute = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"url", url}
            };
            if (mode != "") dictionary.Add("mode", mode);
            if (attribute != "") dictionary.Add("attribute", attribute);
            if (tag != "") dictionary.Add("tag", tag);
            return (DetectResult) HttpGet<DetectResult>(Constants.URL_DETECT, dictionary);
        }
        /// <summary>
        /// 异步检测给定图片(Image)中的所有人脸(Face)的位置和相应的面部属性
        /// 目前面部属性包括性别(gender), 年龄(age)和种族(race)
        /// 若结果的face_id没有被加入任何faceset/person之中，则在72小时之后过期被自动清除
        /// </summary>
        /// <param name="url">待检测图片的URL</param>
        /// <param name="mode">检测模式可以是normal(默认) 或者 oneface 。在oneface模式中，检测器仅找出图片中最大的一张脸。</param>
        /// <param name="attribute">可以是 all(默认) 或者 none或者由逗号分割的属性列表。目前支持的属性包括：gender, age, race</param>
        /// <param name="tag">可以为图片中检测出的每一张Face指定一个不超过255字节的字符串作为tag，tag信息可以通过 /info/get_face 查询</param>
        /// <returns>返回一个DetectAsyncResult，包含一个字符串session_id</returns>
        public AsyncResult Detection_Detect_Async(string url, string mode = "", string attribute = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"url", url}
            };
            if (mode != "") dictionary.Add("mode", mode);
            if (attribute != "") dictionary.Add("attribute", attribute);
            if (tag != "") dictionary.Add("tag", tag);
            dictionary.Add("async", "true");
            return (AsyncResult)HttpGet<AsyncResult>(Constants.URL_DETECT, dictionary);
        }
        /// <summary>
        /// 同步检测给定图片(Image)中的所有人脸(Face)的位置和相应的面部属性
        /// 目前面部属性包括性别(gender), 年龄(age)和种族(race)
        /// 若结果的face_id没有被加入任何faceset/person之中，则在72小时之后过期被自动清除
        /// </summary>
        /// <param name="imgByte">待检测图片的二进制</param>
        /// <param name="mode">检测模式可以是normal(默认) 或者 oneface 。在oneface模式中，检测器仅找出图片中最大的一张脸。</param>
        /// <param name="attribute">可以是 all(默认) 或者 none或者由逗号分割的属性列表。目前支持的属性包括：gender, age, race</param>
        /// <param name="tag">可以为图片中检测出的每一张Face指定一个不超过255字节的字符串作为tag，tag信息可以通过 /info/get_face 查询</param>
        /// <returns>返回一个DetectResult，详细内容请参考FaceEntity.cs代码</returns>
        public DetectResult Detection_DetectImg(byte[] imgByte, string mode = "", string attribute = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object> { };

            if (mode != "") dictionary.Add("mode", mode);
            if (attribute != "") dictionary.Add("attribute", attribute);
            if (tag != "") dictionary.Add("tag", tag);

            return (DetectResult)HttpPost<DetectResult>(Constants.URL_DETECT, dictionary,imgByte);
        }

      
        /// <summary>
        /// 同步检测给定图片(Image)中的所有人脸(Face)的位置和相应的面部属性
        /// 目前面部属性包括性别(gender), 年龄(age)和种族(race)
        /// 若结果的face_id没有被加入任何faceset/person之中，则在72小时之后过期被自动清除
        /// </summary>
        /// <param name="filepath">待检测图片的文件路径</param>
        /// <param name="mode">检测模式可以是normal(默认) 或者 oneface 。在oneface模式中，检测器仅找出图片中最大的一张脸。</param>
        /// <param name="attribute">可以是 all(默认) 或者 none或者由逗号分割的属性列表。目前支持的属性包括：gender, age, race</param>
        /// <param name="tag">可以为图片中检测出的每一张Face指定一个不超过255字节的字符串作为tag，tag信息可以通过 /info/get_face 查询</param>
        /// <returns>返回一个DetectAsyncResult，包含一个字符串session_id</returns>
        public AsyncResult Detection_DetectImg_Async(byte[] imgByte, string mode = "", string attribute = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object> { };

            if (mode != "") dictionary.Add("mode", mode);
            if (attribute != "") dictionary.Add("attribute", attribute);
            if (tag != "") dictionary.Add("tag", tag);

            return (AsyncResult)HttpPost<AsyncResult>(Constants.URL_DETECT, dictionary, imgByte);
        }
        #endregion






        public IResult HttpPost<T>(string url, Dictionary<object, object> h, byte[] byt) where T : new()
        {
            h.Add("api_key", app_key);
            h.Add("api_secret", app_secret);

            IResult result = (IResult)(new T());
            string jsonStr = HttpHelper.CreatePostHttpText(url, h, byt);
            result.CopyObject(Json.Deserialize(jsonStr) as Dictionary<string, object>);
            return result;
        }

        public IResult HttpGet<T>(string url, Dictionary<object, object> h) where T : new()
        {
            h.Add("api_key", app_key);
            h.Add("api_secret", app_secret);

            IResult result = (IResult)(new T());
            string jsonStr = HttpHelper.CreateGetHttpText(url + "?" + ToQueryString(h));
            result.CopyObject(Json.Deserialize(jsonStr) as Dictionary<string, object>);
            return result;
        }

        public static string ToQueryString(IDictionary<object, object> dictionary)
        {
            var sb = new StringBuilder();
            foreach (var key in dictionary.Keys)
            {
                var value = dictionary[key];
                if (value != null)
                {
                    sb.Append(key + "=" + value + "&");
                }
            }
            return sb.ToString().TrimEnd('&');
        }

     
    }
}
