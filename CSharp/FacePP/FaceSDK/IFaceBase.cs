﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{
    public interface IFaceService
    {
        IResult HttpPost<T>(string url,Dictionary<object, object> h, byte[] byt) where T : new();
        IResult HttpGet<T>(string url,Dictionary<object, object> h) where T : new();

    }
    public interface IResult
    {
        void CopyObject(IDictionary<string, object> obj);

    }

}
