﻿using System.Collections.Generic;
using UnityEngine;

namespace FaceppSDK
{
    /// <summary>
    /// 器官位置
    /// </summary>
    public class OrganPosition
    {

        private float x = 0;
        private float y = 0;


        /// <summary>
        /// 器官在脸部的百分比 X
        /// </summary>
        public float X
        {
            get { return x; }
        }

        /// <summary>
        /// 器官在脸部的百分比 Y
        /// </summary>
        public float Y
        {
            get { return y; }
        }
        /// <summary>
        /// 取值0-1
        /// </summary>
        public float X_p
        {
            get { return x * .01f; }
        }
        /// <summary>
        /// 取值0-1
        /// </summary>
        public float Y_p
        {
            get { return y * .01f; }
        }

        /// <summary>
        /// u3d 使用的坐标
        /// </summary>
        public Vector3 Postion
        {
            get
            {
                float x = (this.x - 50) / 100;
                float y = (100 - this.y) / 100;
                return new Vector2(x, y);
            }
        }


        internal void copyObject(IDictionary<string,object> obj)
        {
            x = float.Parse(obj["x"].ToString());
            y = float.Parse(obj["y"].ToString());
        }


        public string ToString()
        {
            return "X-" + X.ToString() + "%   Y-" + Y.ToString() + "%";
        }
    }
}
