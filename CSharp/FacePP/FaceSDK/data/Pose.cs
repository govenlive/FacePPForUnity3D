﻿using System.Collections.Generic;

namespace FaceppSDK
{
    /// <summary>
    /// 包含脸部姿势分析结果，包括pitch_angle, roll_angle, yaw_angle，分别对应抬头，旋转（平面旋转），摇头。单位为角度。
    /// </summary>
    public class Pose
    {
        private float pitch_angle = 0;
        private float roll_angle = 0;
        private float yaw_angle = 0;

        /// <summary>
        /// 抬头角度
        /// </summary>
        public float Pitch_angle
        {
            get { return pitch_angle; }
        }

        /// <summary>
        /// 旋转角度 
        /// </summary>
        public float Roll_angle
        {
            get { return roll_angle; }
        }

        /// <summary>
        /// 摇头角度
        /// </summary>
        public float Yaw_angel
        {
            get { return yaw_angle; }
        }

        internal void copyObject(IDictionary<string,object> obj)
        {

            pitch_angle = float.Parse(((IDictionary<string,object>) obj["pitch_angle"])["value"].ToString());
            roll_angle = float.Parse(((IDictionary<string,object>) obj["roll_angle"])["value"].ToString());
            yaw_angle = float.Parse(((IDictionary<string,object>) obj["yaw_angle"])["value"].ToString());
        }

        public string ToString()
        {
            return "pitch_angle-" + pitch_angle.ToString() + "  roll_angle-" + roll_angle.ToString() + "  yaw_angle" + yaw_angle.ToString();
        }
    }
}
