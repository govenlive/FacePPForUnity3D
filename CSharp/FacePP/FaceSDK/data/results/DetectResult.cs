﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{
    public class AsyncResult : IResult
    {
        private string session_id;
        public string Session_id { get { return session_id; } }
        public virtual void CopyObject(IDictionary<string, object> obj)
        {
            session_id = obj["session_id"].ToString();
        }
    }
    public class DetectResult:AsyncResult
    {
        private int img_width;
        private int img_height;
        private string url;
        private string img_id;
        
        private Face[] faces;

        public Face[] Faces { get { return faces; } }
        public int Img_height { get { return img_height; } }
        public string Img_id { get { return img_id; } }
        public int Img_width { get { return img_width; } }
       
        public string Url { get { return url; } }

        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
            img_width = int.Parse(obj["img_width"].ToString());
            img_height = int.Parse(obj["img_height"].ToString());
            img_id = obj["img_id"].ToString();
            url = obj["url"].ToString();
          

            IList<object> arr = obj["face"] as List<object>;
            faces = new Face[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                Face face = new Face();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }
        }
    }
}
