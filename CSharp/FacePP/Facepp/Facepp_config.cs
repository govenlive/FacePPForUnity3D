﻿
namespace FacePP
{


    /// <summary>
    /// face++的配置
    /// </summary>
    public class Facepp_config
    {
        /// <summary>
        /// 版本号
        /// </summary>
        public const string version = "v2.0";
        /// <summary>
        /// App的Face++ API Key
        /// </summary>
        public static string api_key = "";
        /// <summary>
        /// APP的Face++ API Secret
        /// </summary>
        public static string api_secret = "";

    }
}