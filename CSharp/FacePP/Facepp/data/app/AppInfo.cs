﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP.Facepp.data.app
{
    public interface IAppInfo
    {
        string Name { get; }
        string Description { get; }

        void CopyObject(IDictionary<string,object> obj);
    }
    public  class AppInfo:IAppInfo
    {
        private string name;
        private string description;
        public string Name
        {
            get { return name; }
        }

        public string Description
        {
            get { return description; }
        }

     

       public void CopyObject(IDictionary<string, object> obj)
        {
            name = obj["name"].ToString();
            description = obj["description"].ToString();
        }
    }
}
