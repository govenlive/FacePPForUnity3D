﻿using System.Collections.Generic;

namespace FacePP
{
    /// <summary>
    /// 包含年龄分析结果，value的值为一个非负整数表示估计的年龄, range表示估计年龄的正负区间
    /// </summary>
   public class Age
    {
        private float range = 0;
        private float value = 0;

        /// <summary>
        /// 表示估计年龄的正负区间
        /// </summary>
        public float Range
        {
            get { return range; }
        }

        /// <summary>
        /// 估计年龄
        /// </summary>
        public float Value
        {
            get { return value; }
        }

        internal void copyObject(IDictionary<string,object> obj)
        {
            range =float.Parse(obj["range"].ToString());
            value =float.Parse(obj["value"].ToString());
        }
        public string ToString()
        {
            return (value-range).ToString()+"-"+(value+range).ToString();
        }
    }
}
