﻿using System.Collections.Generic;
using UnityEngine;

namespace FacePP
{
    #region 关键点枚举
    /// <summary>
    /// 关键点名字
    /// </summary>
    public enum LandPointType
    {
        CONTOUR_CHIN,
        CONTOUR_LEFT1,
        CONTOUR_LEFT2,
        CONTOUR_LEFT3,
        CONTOUR_LEFT4,
        CONTOUR_LEFT5,
        CONTOUR_LEFT6,
        CONTOUR_LEFT7,
        CONTOUR_LEFT8,
        CONTOUR_LEFT9,
        CONTOUR_RIGHT1,
        CONTOUR_RIGHT2,
        CONTOUR_RIGHT3,
        CONTOUR_RIGHT4,
        CONTOUR_RIGHT5,
        CONTOUR_RIGHT6,
        CONTOUR_RIGHT7,
        CONTOUR_RIGHT8,
        CONTOUR_RIGHT9,

        //左眼
        LEFT_EYE_BOTTOM,
        LEFT_EYE_CENTER,
        LEFT_EYE_LEFT_CORNER,
        LEFT_EYE_LOWER_LEFT_QUARTER,
        LEFT_EYE_LOWER_RIGHT_QUARTER,
        LEFT_EYE_PUPIL,
        LEFT_EYE_RIGHT_CORNER,
        LEFT_EYE_TOP,
        LEFT_EYE_UPPER_LEFT_QUARTER,
        LEFT_EYE_UPPER_RIGHT_QUARTER,
        LEFT_EYEBROW_LEFT_CORNER,
        LEFT_EYEBROW_LOWER_LEFT_QUARTER,
        LEFT_EYEBROW_LOWER_MIDDLE,
        LEFT_EYEBROW_RIGHT_CORNER,
        LEFT_EYEBROW_UPPER_LEFT_QUARTER,
        LEFT_EYEBROW_UPPER_MIDDLE,
        LEFT_EYEBROW_UPPER_RIGHT_QUARTER,
        //右眼
        RIGHT_EYE_BOTTOM,
        RIGHT_EYE_CENTER,
        RIGHT_EYE_LEFT_CORNER,
        RIGHT_EYE_LOWER_LEFT_QUARTER,
        RIGHT_EYE_LOWER_RIGHT_QUARTER,
        RIGHT_EYE_PUPIL,
        RIGHT_EYE_RIGHT_CORNER,
        RIGHT_EYE_TOP,
        RIGHT_EYE_UPPER_LEFT_QUARTER,
        RIGHT_EYE_UPPER_RIGHT_QUARTER,
        RIGHT_EYEBROW_LEFT_CORNER,
        RIGHT_EYEBROW_LOWER_LEFT_QUARTER,
        RIGHT_EYEBROW_LOWER_MIDDLE,
        RIGHT_EYEBROW_RIGHT_CORNER,
        RIGHT_EYEBROW_UPPER_LEFT_QUARTER,
        RIGHT_EYEBROW_UPPER_MIDDLE,
        RIGHT_EYEBROW_UPPER_RIGHT_QUARTER,



        //口
        MOUTH_LEFT_CORNER,
        MOUTH_LOWER_LIP_BOTTOM,
        MOUTH_LOWER_LIP_LEFT_CONTOUR1,
        MOUTH_LOWER_LIP_LEFT_CONTOUR2,
        MOUTH_LOWER_LIP_LEFT_CONTOUR3,
        MOUTH_LOWER_LIP_RIGHT_CONTOUR1,
        MOUTH_LOWER_LIP_RIGHT_CONTOUR2,
        MOUTH_LOWER_LIP_RIGHT_CONTOUR3,
        MOUTH_UPPER_LIP_LEFT_CONTOUR1,
        MOUTH_UPPER_LIP_LEFT_CONTOUR2,
        MOUTH_UPPER_LIP_LEFT_CONTOUR3,
        MOUTH_UPPER_LIP_RIGHT_CONTOUR1,
        MOUTH_UPPER_LIP_RIGHT_CONTOUR2,
        MOUTH_UPPER_LIP_RIGHT_CONTOUR3,
        MOUTH_LOWER_LIP_TOP,
        MOUTH_RIGHT_CORNER,
        MOUTH_UPPER_LIP_BOTTOM,
        MOUTH_UPPER_LIP_TOP,
        //鼻子
        NOSE_LEFT,
        NOSE_RIGHT,
        NOSE_TIP,
        NOSE_CONTOUR_LOWER_MIDDLE,
        NOSE_CONTOUR_LEFT1,
        NOSE_CONTOUR_LEFT2,
        NOSE_CONTOUR_LEFT3,
        NOSE_CONTOUR_RIGHT1,
        NOSE_CONTOUR_RIGHT2,
        NOSE_CONTOUR_RIGHT3
    }
    #endregion 
   
    /// <summary>
    /// 关键点位置信息
    /// </summary>
   public class FacePostion
    {
        private string name = "";
        private float x = 0;
        private float y = 0;

        /// <summary>
        /// 关键点名字
        /// </summary>
        public string Name { get { return this.name; } }
        /// <summary>
        /// 关键点的x坐标(0-100, 左上角为0)
        /// </summary>
        public float X
        {
            get { return x; }
        }
        /// <summary>
        /// 关键点的y坐标(0-100,左上角为0)
        /// </summary>
        public float Y
        {
            get { return y; }
        }

        /// <summary>
        /// u3d 使用的坐标（百分比）
        /// </summary>
        public Vector2 Postion
        {
            get
            {
                float x = (this.x - 50) / 100;
                float y = (100 - this.y) / 100;
                return new Vector2(x, y);
            }
        }


        internal void CopyObject(IDictionary<string, object> obj,string name="")
        {
            this.name = name;
            x = float.Parse(obj["x"].ToString());
            y = float.Parse(obj["y"].ToString());
        }


    }
}
