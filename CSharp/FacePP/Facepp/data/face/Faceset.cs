﻿using System.Collections.Generic;

namespace FacePP
{
    
    public class Faceset :  FacesetInfo.IFacesetCreated,
        FacesetInfo.IFaceset_getInfo,
        FacesetInfo.IFaceset_setInfo,
        Infos.IFaceset
    {
        private string faceset_id;
        private string faceset_name;
        private string tag;
        private int added_face;
     
        Face[] faces=null;

        public string Tag
        {
            get { return tag; }
        }


        public int Added_face
        {
            get { return added_face; }
        }


        public string Faceset_id
        {
            get { return faceset_id; }
        }

        /// <summary>
        /// faceset的名字
        /// </summary>
        public string Faceset_name
        {
            get { return faceset_name; }
        }

        FacesetInfo.IFace[] FacesetInfo.IFaceset_getInfo.Faces
        {
            get { return faces; }
        }



        // from --/faceset/create
        public void CopyObjectFromCreated(Dictionary<string, object> obj)
        {
            added_face = int.Parse(obj["added_face"].ToString());
            tag = obj["tag"].ToString();
            faceset_name = obj["faceset_name"].ToString();
            faceset_id = obj["faceset_id"].ToString();
        }

        // from --/faceset/set_info
        public void CopyObjectFromFaceSet_SetInfo(IDictionary<string, object> obj)
        {
            tag = obj["tag"].ToString();
            faceset_name = obj["faceset_name"].ToString();
            faceset_id = obj["faceset_id"].ToString();
        }

        // from --/faceset/get_info
        public void CopyObjectFromFaceset_getInfo(Dictionary<string, object> obj)
        {
            IList<object> arr = obj["face"] as List<object>;
            faces = new Face[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                Face face = new Face();
                face.CopyObjectFromFacesetInfo(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }

            tag = obj["tag"].ToString();
            faceset_name = obj["faceset_name"].ToString();
            faceset_id = obj["faceset_id"].ToString();
        }

        // from --/info/get_face
        public void CopyFromGet_face(Dictionary<string, object> obj)
        {
            tag = obj["tag"].ToString();
            faceset_name = obj["faceset_name"].ToString();
            faceset_id = obj["faceset_id"].ToString();
        }
    }
}
