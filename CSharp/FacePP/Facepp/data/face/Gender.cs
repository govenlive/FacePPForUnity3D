﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP
{

    /// <summary>
    /// 
    /// </summary>
    public enum GenderType
    {
        /// <summary>
        /// 人妖
        /// </summary>
        NONE,
        /// <summary>
        /// 男性
        /// </summary>
        MALL,
        /// <summary>
        /// 女性
        /// </summary>
        FEMALL
    }
    /// <summary>
    ///  Gender 包含性别分析
    /// </summary>
    public class Gender
    {

        /// <summary>
        /// 男性
        /// </summary>
        public const string MALL = "Mail";
        /// <summary>
        /// 女性
        /// </summary>
        public const string FEMALL = "Female";
        private float confidence = 0;
        private GenderType value = GenderType.NONE;



        /// <summary>
        /// 男性还是女性  (GenderType)
        /// </summary>
        public GenderType Value
        {
            get { return value; }
        }

        internal void copyObject(IDictionary<string,object> obj)
        {
            confidence = float.Parse(obj["confidence"].ToString());
            if(confidence>20)
            {
                switch (obj["value"].ToString())
                {
                    case "Male":
                        value = GenderType.MALL;
                        break;
                    case "Female":
                        value = GenderType.FEMALL;
                        break;
                    default:
                        value = GenderType.NONE;
                        break;
                }
              
            }
            else
            {
                value = GenderType.NONE;
            }
           
        }


    }
}
