﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP
{

    public interface IResult
    {
        void CopyObject(IDictionary<string, object> obj);

    }
}
