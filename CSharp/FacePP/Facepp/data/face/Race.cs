﻿using System.Collections.Generic;

namespace FacePP
{
    public enum RaceType
    {
        /// <summary>
        /// 外星人
        /// </summary>
        NONE,
        /// <summary>
        /// 高尚的中国人
        /// </summary>
        ASIAN,
        /// <summary>
        /// 美国婊
        /// </summary>
        WHITE,
        /// <summary>
        /// 非洲~~~~
        /// </summary>
        BLACK
    }
    /// <summary>
    /// 包含人种分析结果，value的值为Asian/White/Black, confidence表示置信度
    /// </summary>
   public class Race
    {
        private float confidence = 0;
        private RaceType value = RaceType.NONE;
        /// <summary>
        /// 种族  
        /// </summary>
        public RaceType Value
        {
            get { return value; }
        }

        internal void copyObject(IDictionary<string, object> obj)
        {
            confidence = float.Parse(obj["confidence"].ToString());
            if (confidence > 20)
            {
                switch ((string)obj["value"])
                {
                    case "Asian":
                        value = RaceType.ASIAN;
                        break;
                    case "White":
                        value = RaceType.WHITE;
                        break;
                    case "Black":
                        value = RaceType.BLACK;
                        break;

                    default:
                        value = RaceType.NONE;
                        break;
                }

            }
            else
            {
                value = RaceType.NONE;
            }
        }
    }
}
