﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP
{
    public interface IGroup
    {
        /// <summary>
        /// 相应group的id
        /// </summary>
        string Group_id { get; }
        /// <summary>
        /// 相应group的name
        /// </summary>
        string Group_name { get; }
        /// <summary>
        /// group相关的tag
        /// </summary>
        string Tag { get; }

        void CopyObject(IDictionary<string,object> obj);
    }

    public class Group:IGroup
    {
        private string group_id;
        private string group_name;
        private string tag;
        /// <summary>
        /// id
        /// </summary>
        public string Group_id
        {
            get { return group_id; }
        }
        /// <summary>
        /// name
        /// </summary>
        public string Group_name
        {
            get { return group_name; }
        }
        /// <summary>
        /// tag
        /// </summary>
        public string Tag
        {
            get { return tag; }
        }


        public virtual void CopyObject(IDictionary<string, object> obj)
        {
            group_id = obj["group_id"].ToString();
            group_name = obj["group_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }
}
