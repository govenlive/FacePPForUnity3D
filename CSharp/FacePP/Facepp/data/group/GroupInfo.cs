﻿using System.Collections.Generic;

namespace FacePP
{

    public interface IGroupCreated : IGroup
    {
        int Added_person{ get; }

     
    }
    public interface IGroupGetInfoed : IGroup
    {
        IDictionary<int, IPerson> Persons { get; }
    }

    public class GroupGetInfoed : Group, IGroupGetInfoed
    {
        private IDictionary<int, IPerson> persons;
        public IDictionary<int, IPerson> Persons
        {
            get { return persons; }
        }


        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);

            persons = new Dictionary<int, IPerson>();
            IList<object> arr = obj["person"] as List<object>;
            for (int i = 0; i < arr.Count; i++)
            {
                IPerson person = new Person();
                person.CopyObject(arr[i] as Dictionary<string, object>);
                persons[i] = person;
            }

        }
    }
    public class GroupCreated : Group, IGroupCreated
    {
    
        private int added_person=0;
       
        public int Added_person
        {
            get { return added_person; }
        }
        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
             added_person = int.Parse(obj["added_person"].ToString());
        }

    }
}
