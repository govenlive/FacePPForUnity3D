﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP
{
    public interface IImg
    {
        IDictionary<int, IFaceBasic> Faces { get; }

        string Img_id { get; }
        string Url { get; }

      
        void CopyObject(IDictionary<string, object> obj);
    }
    public class Img:IImg
    {
        private IDictionary<int, IFaceBasic> faces;
        private string img_id;
        private string url;
     

        public IDictionary<int, IFaceBasic> Faces
        {
            get { return faces; }
        }

        public string Img_id
        {
            get { return img_id; }
        }

        public string Url
        {
            get { return url; }
        }


        public virtual void CopyObject(IDictionary<string, object> obj)
        {
            faces = new Dictionary<int, IFaceBasic>();
            IList<object> faceArr = obj["face"] as List<object>;

            for (int i = 0; i < faceArr.Count; i++)
            {
                IFaceBasic face = new Face();
                face.CopyObject(faceArr[i] as Dictionary<string, object>);
                faces[i] = face;
            }
            img_id = obj["img_id"].ToString();
            if (obj["url"] != null)
                url = obj["url"].ToString();
            //

        }
    }
}
