﻿using System.Collections.Generic;

namespace FacePP
{
    public interface IInfo : IInfoBase
    {
        /// <summary>
        /// 脸部集合
        /// </summary>
        IDictionary<int, IFaceBasic> Faces { get; }


    }
    public class Info:InfoBase,IInfo
    {
        private IDictionary<int, IFaceBasic> faces;

        /// <summary>
        /// 脸部集合
        /// </summary>
        public IDictionary<int, IFaceBasic> Faces
        {
            get { return faces; }
        }

        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
            if (obj["face"] != null)
            {
                faces = new Dictionary<int, IFaceBasic>();
                IList<object> arr = obj["face"] as List<object>;
                for (int i = 0; i < arr.Count; i++)
                {
                    IFaceBasic face = new Face();
                    face.CopyObject(arr[i] as Dictionary<string, object>);
                    faces[i] = face;
                }
            }
        }
    }
}
