﻿using System.Collections.Generic;

namespace FacePP
{
    /// <summary>
    /// info
    /// </summary>
    public interface IInfoBase
    {
        /// <summary>
        /// id
        /// </summary>
        string Id { get; }
        /// <summary>
        /// name
        /// </summary>
        string Name { get; }
        /// <summary>
        /// tag
        /// </summary>
        string Tag { get; }

        void CopyObject(IDictionary<string, object> obj);
    }


    public class InfoBase : IInfoBase
    {
        private string id;
        private string name;
        private string tag;
      

        public string Id
        {
            get { return id; }
        }

        public string Name
        {
            get { return name; }
        }

        public string Tag
        {
            get { return tag; }
        }

        public virtual void CopyObject(IDictionary<string, object> obj)
        {
            
            id = obj["id"].ToString();
            name = obj["name"].ToString();
            tag = obj["tag"].ToString();
            
        }

       
    }
}
