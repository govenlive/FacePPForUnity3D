﻿using System.Collections.Generic;

namespace FacePP
{
    public interface IAddedState : IState
    {
        /// <summary>
        /// 成功加入的数量
        /// </summary>
        int Added { get; }
    }
    public class AddedState : StateBase, IAddedState
    {
        private int added;
        /// <summary>
        /// 成功加入的数量
        /// </summary>
        public int Added
        {
            get { return added; }
        }


        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
            added = int.Parse(obj["added"].ToString());
        }
       
    }
}
