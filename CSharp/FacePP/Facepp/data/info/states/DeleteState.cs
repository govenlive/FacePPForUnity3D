﻿using System.Collections.Generic;

namespace FacePP
{
    public interface IDeleteState:IState
    {
        /// <summary>
        /// 删除了多少个
        /// </summary>
        int Deleted { get; }
    }
    public class DeleteState:StateBase,IDeleteState
    {
        private int deleted;
        /// <summary>
        /// 删除了多少个
        /// </summary>
        public int Deleted
        {
            get { return deleted; }
        }

        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
            deleted =int.Parse( obj["deleted"].ToString());
        }
    }
}
