﻿using System.Collections.Generic;

namespace FacePP
{
    public interface IRemovedState:IState
    {
        /// <summary>
        /// 删除的数量
        /// </summary>
        int Removed { get; }
    }

    public class RemovedState:StateBase,IRemovedState
    {
        private int removed;
        /// <summary>
        /// 删除的数量
        /// </summary>
        public int Removed
        {
            get { return removed; }
        }
        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
            removed = int.Parse( obj["removed"].ToString());
        }
    }
}
