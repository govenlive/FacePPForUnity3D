﻿using System.Collections.Generic;

namespace FacePP
{
    /// <summary>
    /// 状态
    /// </summary>
    public interface IState
    {
        /// <summary>
        /// 成功
        /// </summary>
        bool Success { get; }

        void CopyObject(IDictionary<string, object> obj);
    }

    public class StateBase:IState
    {
        private bool success;
   
        /// <summary>
        /// 成功了?
        /// </summary>
        public bool Success
        {
            get { return success; }
        }

        public virtual void CopyObject(IDictionary<string, object> obj)
        {
            success = bool.Parse(obj["success"].ToString());
        }
    }


    
}
