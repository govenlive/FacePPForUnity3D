﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP
{
    /// <summary>
    /// 获取Person
    /// </summary>
    public interface IPerson
    {
        /// <summary>
        /// 相应person的id
        /// </summary>
        string Persion_id { get; }

        /// <summary>
        /// 相应person的name
        /// </summary>
        string Persion_name { get; }

        /// <summary>
        /// person相关的tag
        /// </summary>
        string Tag { get; }

        void CopyObject(IDictionary<string, object> obj);
    }
    /// <summary>
    /// person
    /// </summary>
   public  class Person:IPerson 
    {
       private string persion_id;
       private string persion_name;
       private string tag;

        /// <summary>
        /// id
        /// </summary>
        public string Persion_id
        {
            get { return persion_id; }
        }
       /// <summary>
       /// name
       /// </summary>
        public string Persion_name
        {
            get { return persion_name; }
        }
       /// <summary>
       /// tag
       /// </summary>
        public string Tag
        {
            get { return tag; }
        }

        public virtual void CopyObject(IDictionary<string, object> obj)
        {
            persion_id = obj["persion_id"].ToString();
            persion_name = obj["persion_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }
}
