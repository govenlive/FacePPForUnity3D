﻿using System.Collections.Generic;

namespace FacePP
{
    public interface IPersonCreated:IPerson
    {
        /// <summary>
        /// 成功加入的face数量
        /// </summary>
        int Added_face { get; }
        /// <summary>
        /// 成功被加入的group数量
        /// </summary>
        int Added_group { get; }
    }
    public interface IPersonGetInfoed : IPerson
    {
        /// <summary>
        /// 获取到的face集合
        /// </summary>
        IDictionary<int, IFaceBasic> Faces { get; }
        /// <summary>
        /// 获取到的group集合
        /// </summary>
        IDictionary<int, IGroup> Groups { get; }
    }

    public class PersonGetInfoed : Person, IPersonGetInfoed
    {
        private IDictionary<int, IFaceBasic> faces;
        private IDictionary<int, IGroup> groups;


        public IDictionary<int, IFaceBasic> Faces
        {
            get { return faces; }
        }

        public IDictionary<int, IGroup> Groups
        {
            get { return groups; }
        }

        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
            int i = 0;

            IList<object> arr = obj["face"] as List<object>;         
            faces = new Dictionary<int, IFaceBasic>();
            for (i=0; i < arr.Count; i++)
            {
                IFaceBasic face = new Face();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }
            groups=new Dictionary<int,IGroup>();
            arr=obj["group"] as List<object>;
            for(i=0;i<arr.Count;i++){
                IGroup group = new Group();
                group.CopyObject(arr[i] as Dictionary<string, object>);
                groups[i] = group;
            }
        }
    }
    public class PersonCreated : Person, IPersonCreated
    {
        private int added_face=0;
        private int added_group=0;

        public int Added_face
        {
            get { return added_face; }
        }

        public int Added_group
        {
            get { return added_group; }
        }

        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
            if (obj["added_face"] != null)
                added_face = int.Parse(obj["added_face"].ToString());
            if (obj["added_group"] != null)
                added_group = int.Parse(obj["added_group"].ToString());
        }
       
    }

}
