﻿using System.Collections.Generic;

namespace FacePP.AA
{
    public interface ISessionBase:IImg
    {
     

        float Img_height { get; }

        string Session_id { get; }
        float Img_width { get; }

     

    }
    public interface ISession : ISessionBase
    {
        int Create_time { get; }
        int Finish_time { get; }
        string Status { get; }
    }

    public class Session : SessionBase, ISession
    {
        private string status;

        private int create_time;
        private int finish_time;

        public string Status
        {
            get { return status; }
        }


        public int Create_time
        {
            get { return create_time; }
        }

        public int Finish_time
        {
            get { return finish_time; }
        }

        public override void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
                status = obj["status"].ToString();
                create_time = int.Parse(obj["create_time"].ToString());
                finish_time = int.Parse(obj["finish_time"].ToString());
        }


    }
    public class SessionBase:Img,ISessionBase
    {
        private string session_id;
        private float img_width;
        private float img_height;
       

        public string Session_id
        {
            get { return session_id; }
        }

        public float Img_width
        {
            get { return img_width; }
        }

        public float Img_height
        {
            get { return img_height; }
        }


       

        public virtual void CopyObject(IDictionary<string, object> obj)
        {
            base.CopyObject(obj);
            session_id = obj["session_id"].ToString();
         
            img_width = float.Parse(obj["img_width"].ToString());
            img_height = float.Parse(obj["img_height"].ToString());
        }
    }
}
