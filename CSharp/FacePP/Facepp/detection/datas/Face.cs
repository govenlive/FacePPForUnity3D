﻿using System.Collections.Generic;

namespace FacePP.Detection
{
    public interface IFace
    {
        /// <summary>
        /// 被检测出的每一张人脸都在Face++系统中的标识符
        /// </summary>
        string Face_id { get; }
        /// <summary>
        /// 相关的Tag
        /// </summary>
        string Tag { get; }
        /// <summary>
        /// 脸部的一些信息
        /// </summary>
        Attribute Attribute { get; }
        /// <summary>
        /// 脸部的一些器官位置
        /// </summary>
        Position Position { get; }

        void CopyObjectFromDetection(IDictionary<string, object> obj);

    }
}
