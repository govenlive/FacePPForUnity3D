﻿using System.Collections.Generic;

namespace FacePP.Detection
{
    public interface ISession
    {
        /// <summary>
        /// 请求图片的宽度
        /// </summary>
        float Img_width { get; }
        /// <summary>
        /// 请求图片的高度
        /// </summary>
        float Img_height { get; }
        /// <summary>
        /// Face++系统中的图片标识符，用于标识用户请求中的图片
        /// </summary>
        string Img_id { get; }
        /// <summary>
        /// 相应请求的session标识符，可用于结果查询
        /// </summary>
        string Session_id { get; }
        /// <summary>
        /// 请求中图片的url
        /// </summary>
        string Url { get; }
        /// <summary>
        /// 被检测出的人脸的列表
        /// </summary>
        IFace[] Faces { get; }

      
        string ToString();
    }
    public class Session:ISession,IResult
    {
        private float img_width;
        private float img_height;
        private string img_id;
        private string session_id;
        private string url;
        private IFace[] faces;


        public float Img_width
        {
            get { return img_width; }
        }

        public float Img_height
        {
            get { return img_height; }
        }

        public string Img_id
        {
            get { return img_id; }
        }

        public string Session_id
        {
            get { return session_id; }
        }

        public string Url
        {
            get { return url; }
        }

        public IFace[] Faces
        {
            get { return faces; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {

            session_id = obj["session_id"].ToString();

            img_width = float.Parse(obj["img_width"].ToString());

            img_height = float.Parse(obj["img_height"].ToString());

            img_id = obj["img_id"].ToString();
          
            url =obj["url"]!=null? obj["url"].ToString():"";

            IList<object> arr = obj["face"] as List<object>;
            faces = new IFace[arr.Count];
            for(int i=0;i<arr.Count;i++)
            {
                IFace face = new Face();
                face.CopyObjectFromDetection(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }
          

        }

        public override string ToString()
        {
            return "img_width: " + img_width + " img_height: " + img_height + " img_id: " + img_id + "  url: " + url + "  session_id: " + session_id+" face "+faces.Length.ToString();
        }

    }
}
