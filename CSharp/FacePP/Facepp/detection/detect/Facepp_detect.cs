﻿using System.Collections;
using System.Collections.Generic;
using MiniJSON;


namespace FacePP.Detection
{

    public interface IFacePP_detect
    {
        ISession DetectSession(string imgUrl, ModeType modeType = ModeType.NORMAL, bool async = false, string tag = "A");
        Session DetectSession(byte[] byt, ModeType modeType = ModeType.NORMAL, bool async = false, string tag = "");
        ISession DetectSession(string jsonStr);
  
    
    }
    /// <summary>
    /// Face++ detect api(检测人脸信息)
    /// </summary>
    public class Facepp_detect:IFacePP_detect
    {
     
        #region 单例
        private static Facepp_detect _instance = null;
        /// <summary>
        /// 实现单例模式
        /// </summary>
        public static Facepp_detect instance
        {

            get
            {
                if (_instance == null)
                {
                    _instance = new Facepp_detect();
                }
                return _instance;
            }
        }
        #endregion
      
        private const string URL = "http://apicn.faceplusplus.com/v2/detection/detect";


        #region 获取face++ 数据(通过图片链接)
        /// <summary>
        ///  获取face++ 数据 ,调用之前请先设置好api_key和api_secret
        /// </summary>
        /// <param name="imgUrl">待检测图片的URL </param>
        /// <param name="mode">检测模式可以是normal(默认) 或者 oneface 。在oneface模式中，检测器仅找出图片中最大的一张脸。</param>
        /// <param name="async">如果置为true，该API将会以异步方式被调用；也就是立即返回一个session id，稍后可通过/info/get_session查询结果。默认值为false。</param>
        /// <param name="tag">可以为图片中检测出的每一张Face指定一个不包含^@,&=*'"等非法字符且不超过255字节的字符串作为tag，tag信息可以通过 /info/get_face 查询</param>
        /// <param name="api_key">api_key </param>
        /// <param name="api_secret">api_secret</param> 
        /// <returns>Session 数据</returns>
        public ISession DetectSession(string imgUrl, ModeType modeType, bool async = false, string tag = "A")
        {
            string mode = "";
            if (modeType == ModeType.NORMAL)
            {
                mode = "normal";
            }
            else if (modeType == ModeType.ONEFACE)
            {
                mode = "oneface";
            }
            //创建哈希表
            IDictionary<object, object> h = new Dictionary<object, object>();
            h.Add("api_key",FacePP.Facepp_config.api_key);
            h.Add("api_secret", FacePP.Facepp_config.api_secret);
            h.Add("url", imgUrl);
            h.Add("mode", mode);
            h.Add("async", async);
            h.Add("tag", tag);
            h.Add("attribute", "glass,pose,gender,age,race,smiling");

            //获取服务器返回的信息
            return DetectSession(HttpHelper.CreatePostHttpText(URL, h, null));
        }


    


        /// <summary>
        ///  获取face++ 数据 ,调用之前请先设置好api_key和api_secret
        /// </summary>
        /// <param name="byt">待检测图片的二进制</param>
        /// <param name="mode">检测模式可以是normal(默认) 或者 oneface 。在oneface模式中，检测器仅找出图片中最大的一张脸。</param>
        /// <param name="async">如果置为true，该API将会以异步方式被调用；也就是立即返回一个session id，稍后可通过/info/get_session查询结果。默认值为false。</param>
        /// <param name="tag">可以为图片中检测出的每一张Face指定一个不包含^@,&=*'"等非法字符且不超过255字节的字符串作为tag，tag信息可以通过 /info/get_face 查询</param>
        /// <returns>Session 数据</returns>
        public Session DetectSession(byte[] byt, ModeType modeType = ModeType.NORMAL, bool async = false, string tag = "")
        {
            string mode = "";
            if (modeType == ModeType.NORMAL)
            {
                mode = "normal";
            }
            else if (modeType == ModeType.ONEFACE)
            {
                mode = "oneface";
            }

            //创建哈希表
            Dictionary<object, object> h = new Dictionary<object, object>();
           
            h.Add("mode",mode);
            h.Add("async", async);
            h.Add("tag", tag);
            h.Add("attribute", "glass,pose,gender,age,race,smiling");

            //获取服务器信息
          //  return DetectSession(HttpHelper.CreatePostHttpText(URL, h, byt));

            return (Session)HttpPost<Session>(h, byt);
        }




        private IResult HttpPost<T>(Dictionary<object, object> h, byte[] byt) where T : new()
        {
            h.Add("api_key", FacePP.Facepp_config.api_key);
            h.Add("api_secret", FacePP.Facepp_config.api_secret);

            IResult result =(IResult)(new T());
            string jsonStr = HttpHelper.CreatePostHttpText(URL, h, byt);
           // result.CopyObject(Json.Deserialize(jsonStr) as Dictionary<string,object>);
            return result;
        }


   



        public ISession DetectSession(string jsonStr)
        {
            this.jsonStr = jsonStr;
            try
            {
                ISession session = new Session();
              
             //   session.CopyObject(Json.Deserialize(jsonStr) as Dictionary<string, object>);
              
                return session;
            }
            catch (System.Exception)
            {
               
             //   throw;
            }

            return null;
        }
     
        private string jsonStr = "";
        public string JsonStr { get { return jsonStr; } }

    }
        #endregion


       

   
    /// <summary>
    /// 检测模式
    /// </summary>
    
}
namespace FacePP
{
    public enum ModeType
    {
        /// <summary>
        /// 默认
        /// </summary>
        NORMAL,
        /// <summary>
        /// 一张脸模式
        /// </summary>
        ONEFACE
    }
}