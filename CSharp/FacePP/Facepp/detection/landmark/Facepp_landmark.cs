﻿using System;
using System.Collections.Generic;
using MiniJSON;



namespace FacePP.Detection
{
	    
    /// <summary>
    /// 关键点数量
    /// </summary>
        public enum FacePointType
        {
            /// <summary>
            /// 25 关键点模式 
            /// </summary>
             P25,
            /// <summary>
            /// 83关键点模式
            /// </summary>
            P83
        }

    /// <summary>
        /// 检测给定人脸(Face)相应的面部轮廓，五官等关键点的位置，包括25点和83点两种模式。
    /// </summary>
		public class Facepp_landmark
		{
				private static Facepp_landmark _instance = null;

            /// <summary>
            /// 单例模式
            /// </summary>
				public static Facepp_landmark instance {
						get {
								if (_instance == null)
										_instance = new Facepp_landmark ();
								return _instance;
						}
				}

                private const string API_URL = "http://api.faceplusplus.com/detection/landmark";

				/// <summary>
                /// Gets the landmarkdata from faceID.
				/// </summary>
				/// <param name="face_id"></param>
				/// <param name="pType"></param>
				/// <returns></returns>
                public FaceLandMarkData GetLandMarkDataFromFaceID(string face_id, FacePointType pType = FacePointType.P25)
                {
                    string p_type = "";
                    if (pType == FacePointType.P25)
                    {
                        p_type = "25p";
                    }
                    else if (pType == FacePointType.P83)
                    {
                        p_type = "83p";
                    }



                    string sendUrl = API_URL + "?api_key=" +Facepp_config.api_key+ "&" +
                            "api_secret=" + Facepp_config.api_secret + "&" +
                            "face_id=" + face_id + "&" +
                            "type=" + p_type;
                    //获取服务器返回的信息
                    string json = HttpHelper.CreateGetHttpText(sendUrl);
                    return GetLandMarkDataFromJsonStr(json);
                }
      


                /// <summary>
                /// 此api可以自己传入拿到的face++ json文本解析为对应的数据
                /// </summary>
                /// <param name="json"></param>
                /// <returns></returns>
                public FaceLandMarkData GetLandMarkDataFromJsonStr(string json)
                {
                    try
                    {
                        FaceLandMarkData d = new FaceLandMarkData();
                        object jsonObj = Json.Deserialize(json);
                        d.copyObject(jsonObj as IDictionary<string, object>);
                        return d;
                    }
                    catch (Exception)
                    {

                        return null;
                    }
                }

                /// <summary>
                /// Gets the landmarkdata from imgUrl.
                /// </summary>
                /// <param name="imgUrl"></param>
                /// <param name="pType"></param>
                /// <returns></returns>
                public FaceLandMarkData GetLandMarkDataFromDetect(string imgUrl, FacePointType pType = FacePointType.P25)
                {
                    ISession session = Facepp_detect.instance.DetectSession(imgUrl, ModeType.ONEFACE, false);
                    if (session.Faces.Length > 0)
                    {
                        return GetLandMarkDataFromFaceID(session.Faces[0].Face_id, pType);
                    }
                    return new FaceLandMarkData();
                }
			
              /// <summary>
                ///  Gets the landmarkdata from img bytes.
              /// </summary>
              /// <param name="byt"></param>
              /// <param name="pType"></param>
              /// <returns></returns>
                public FaceLandMarkData GetLandMarkDataFromImgByte(byte[] byt, FacePointType pType = FacePointType.P25)
                {
                    ISession session = Facepp_detect.instance.DetectSession(byt);

                    if (session.Faces.Length> 0)
                    {
                        return GetLandMarkDataFromFaceID(session.Faces[0].Face_id, pType);
                    }
                    return new FaceLandMarkData();
                }
             

		}



	  

   
      

}
