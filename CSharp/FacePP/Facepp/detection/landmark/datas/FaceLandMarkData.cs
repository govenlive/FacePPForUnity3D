﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP
{
    /// <summary>
    /// 面部轮廓数据
    /// </summary>
   public class FaceLandMarkData
    {
        private string session_id = "";

        private IDictionary<int, LandMark> landMarks;





        /// <summary>
        /// 包含详细关键点分析结果，包含多个关键点的坐标。
        /// </summary>
        /// <value>The landmark.</value>
        public LandMark Landmark
        {
            get
            {
                if (landMarks != null && landMarks.Count > 0)
                {
                    return landMarks[0];
                }


                return null;
            }
        }
        /// <summary>
        /// 相应请求的session标识符，可用于结果查询
        /// </summary>
        /// <value>The session_id.</value>
        public string Session_id
        {
            get { return session_id; }
        }

        internal void copyObject(IDictionary<string, object> obj)
        {
            landMarks = new Dictionary<int, LandMark>();
            List<object> results = obj["result"] as List<object>;
            if (results.Count > 0)
            {
                int a = 0;
                foreach (object lm in results)
                {
                    IDictionary<string, object> idc = lm as IDictionary<string, object>;
                    LandMark landmark = new LandMark();
                    landmark.copyObject(idc);
                    landMarks[a] = landmark;
                    a++;
                }
            }
            session_id = (string)obj["session_id"];
        }
    }
}
