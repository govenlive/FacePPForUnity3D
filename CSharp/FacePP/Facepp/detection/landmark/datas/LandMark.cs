﻿using System;
using System.Collections.Generic;

namespace FacePP
{
    /// <summary>
    ///  包含详细关键点分析结果，包含多个关键点的坐标。
    /// </summary>
    public class LandMark
    {
        private string face_id = "";
        private IDictionary<LandPointType, FacePostion> points = null;


        /// <summary>
        /// 人脸在Face++系统中的标识符
        /// </summary>
        public string Face_id
        {
            get { return face_id; }
        }

        /// <summary>
        /// 关键点集合,通过key 枚举LandPointType
        /// </summary>
        public IDictionary<LandPointType, FacePostion> Points
        {
            get { return points; }
        }
        /// <summary>
        /// 根据type返回包含 x y 的P 
        /// </summary>
        /// <returns>The p.</returns>
        /// <param name="type">Type.(LandMarkType)</param>
        public FacePostion GetPoint(LandPointType type)
        {
            if (points != null)
            {
                return points[type];
            }
            return null;
        }

        internal void copyObject(IDictionary<string, object> obj)
        {
            points = new Dictionary<LandPointType, FacePostion>();
            face_id = obj["face_id"].ToString();
            IDictionary<string, object> landmark = obj["landmark"] as IDictionary<string, object>;

            foreach (LandPointType tum in Enum.GetValues(typeof(LandPointType)))
            {
                string tName = tum.ToString();
                tName = tName.ToLower();//转换小小写
                if (landmark[tName] != null)
                {
                    FacePostion k = new FacePostion();
                    k.CopyObject(landmark[tName] as IDictionary<string, object>, tName);
                    points[tum] = k;
                }

            }

        }
    }
}
