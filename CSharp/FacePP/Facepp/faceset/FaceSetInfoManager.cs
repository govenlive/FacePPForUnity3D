﻿using System;
using System.Collections.Generic;
using MiniJSON;

namespace FacePP.FacesetInfo
{
    public interface IFaceSetInfoManager
    {
        #region create
     

        /// <summary>
        /// 通过faceset_name、face_id 和 tag 信息创建一个Facesetinfo
        /// </summary>
        /// <param name="face_idArr">face_id 数组,表示将这些Face加入到该Faceset中</param>
        /// <param name="faceset_name">>Faceset的Name信息，必须在App中全局唯一。Name不能包含^@,&=*'"等非法字符，且长度不得超过255。Name也可以不指定，此时系统将产生一个随机的name。</param>
        /// <param name="tag">Faceset相关的tag，不需要全局唯一，不能包含^@,&=*'"等非法字符，长度不能超过255。</param>
        /// <returns></returns>
        IFacesetCreated Create_Info(string[] face_ids, string faceset_name = "", string tag = "");
        #endregion

        #region Delete
        IDeleteState Detele_FromName(string[] faceset_names);
        /// <summary>
        /// 删除一组Faceset
        /// </summary>
        /// <param name="faceset_id">用逗号隔开的待删除的faceset id列表</param>
        /// <returns></returns>
        IDeleteState Detele_FromId(string[] faceset_ids);
        #endregion

        #region added
        /// <summary>
        /// 将一组Face加入到一个Faceset中。
        /// (一个Faceset最多允许包含10000个Face。)
        /// </summary>
        /// <param name="faceset_name">相应Faceset的name</param>
        /// <param name="face_id">一组用逗号分隔的face_id,表示将这些Face加入到相应Faceset中。</param>
        /// <returns></returns>
        IAddedState Added_FromName(string faceset_name, string[] face_ids);
        /// <summary>
        /// 将一组Face加入到一个Faceset中。
        /// (一个Faceset最多允许包含10000个Face。)
        /// </summary>
        /// <param name="faceset_id">相应Faceset的id</param>
        /// <param name="face_id">一组用逗号分隔的face_id,表示将这些Face加入到相应Faceset中。</param>
        /// <returns></returns>
        IAddedState Added_FromId(string faceset_id, string[] face_ids);  
        #endregion

        #region removed
        
      
        /// <summary>
        /// 删除Faceset中的一个或多个Face
        /// </summary>
        /// <param name="faceset_name">相应Faceset的name</param>
        /// <param name="face_id">一组用逗号分隔的face_id,表示将这些Face加入到相应Faceset中。</param>
        /// <returns></returns>
        IRemovedState Removed_FromName(string faceset_name, string[] face_ids);
        /// <summary>
        /// 删除Faceset中全部Face
        /// </summary>
        /// <param name="faceset_name"></param>
        /// <returns></returns>
        IRemovedState Removed_FromName(string faceset_name);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="faceset_id">相应Faceset的id</param>
        /// <param name="face_id">一组用逗号分隔的face_id,表示将这些Face加入到相应Faceset中。</param>
        /// <returns></returns>
        IRemovedState Removed_FromId(string faceset_id, string[] face_ids);
        /// <summary>
        /// 删除Faceset中全部Face
        /// </summary>
        /// <param name="faceset_id"></param>
        /// <returns></returns>
        IRemovedState Removed_FromId(string faceset_id);
        #endregion

        #region set     
        /// <summary>
        /// 设置faceset的name和tag
        /// </summary>
        /// <param name="faceset_id">相应faceset的id</param>
        /// <param name="name">新的name(""时为不改变)</param>
        /// <param name="tag">新的tag(""时为不改变)</param>
        /// <returns></returns>
        IFaceset_getInfo SetInfo_FromId(string faceset_id, string name = "", string tag = "");
        /// <summary>
        /// 设置faceset的name和tag
        /// </summary>
        /// <param name="faceset_name">相应faceset的name</param>
        /// <param name="name">新的name(""时为不改变)</param>
        /// <param name="tag">新的tag(""时为不改变)</param>
        /// <returns></returns>
        IFaceset_getInfo SetInfo_FromName(string faceset_name, string name = "", string tag = "");
        #endregion

        #region get
        
       
        /// <summary>
        /// 获取一个faceset的信息, 包括name, id, tag, 以及相关的face等信息
        /// </summary>
        /// <param name="faceset_id">相应faceset的id</param>
        /// <returns></returns>
        IFaceset_setInfo GetInfo_FromId(string faceset_id);
        /// <summary>
        /// 获取一个faceset的信息, 包括name, id, tag, 以及相关的face等信息
        /// </summary>
        /// <param name="faceset_name">相应faceset的id</param>
        /// <returns></returns>
       // IFacesetGetInfoed GetInfo_FromName(string faceset_name);
        #endregion
   


    }
    public class FaceSetInfoManager:IFaceSetInfoManager
    {
        private const string CREATE_URL = "https://apicn.faceplusplus.com/v2/person/create";
        private const string DELETE_URL = "https://apicn.faceplusplus.com/v2/faceset/delete";
        private const string ADD_URL = "https://apicn.faceplusplus.com/v2/faceset/add_face";
        private const string REMOVE_URL = "https://apicn.faceplusplus.com/v2/faceset/remove_face";
        private const string SETINFO_URL = "https://apicn.faceplusplus.com/v2/faceset/set_info";
        private const string GETINFO_URL = "https://apicn.faceplusplus.com/v2/faceset/get_info";
        ///    ///   ///


        IFacesetCreated Create_Info(string face_id = "", string faceset_name = "", string tag = "")
        {
            string sendUrl=CREATE_URL+"?api_key="+Facepp_config.api_key+"&api_secret="+Facepp_config.api_secret;
            if (face_id != "") sendUrl += "&face_id=" + face_id;
            if (faceset_name != "") sendUrl += "&faceset_name=" + faceset_name;
            if (tag != "") sendUrl += "&tag=" + tag;

           
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IFacesetCreated info = new Faceset();
              //  info.CopyObject(obj as Dictionary<string, object>);
                return info;
            }
            catch (Exception)
            {
                
               // throw;
            }
            return null;
        }

        public IFacesetCreated Create_Info(string[] face_ids, string faceset_name = "", string tag = "")
        {
            string face_id = String.Join(",", face_ids);
            return Create_Info(face_id, faceset_name, tag);
        }


        private IDeleteState detele(string sendUrl)
        {
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IDeleteState state = new DeleteState();
                state.CopyObject(obj as Dictionary<string, object>);
                return state;
            }
            catch (Exception)
            {

                // throw;
            }
            return null;
        }
        IDeleteState Detele_FromName(string faceset_name)
        {
            string sendUrl = DELETE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_name=" + faceset_name;
            return detele(sendUrl);
        }

      
        public IDeleteState Detele_FromName(string[] faceset_names)
        {
            return Detele_FromName(String.Join(",", faceset_names));
        }


      
        IDeleteState Detele_FromId(string faceset_id)
        {
            string sendUrl = DELETE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_id=" + faceset_id;
            return detele(sendUrl);
        }

        public IDeleteState Detele_FromId(string[] faceset_ids)
        {
            return Detele_FromId(String.Join(",", faceset_ids));
        }


        private IAddedState added(string sendUrl)
        {
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IAddedState state = new AddedState();
                state.CopyObject(obj as Dictionary<string, object>);
                return state;
            }
            catch (Exception)
            {

                //  throw;
            }
            return null;
        }

        IAddedState Added_FromName(string faceset_name, string face_id)
        {
            string sendUrl =ADD_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_name=" + faceset_name + "&face_id=" + face_id;
            return added(sendUrl);
        }

        public IAddedState Added_FromName(string faceset_name, string[] face_ids)
        {
            return Added_FromName(faceset_name, String.Join(",", face_ids));
        }

        IAddedState Added_FromId(string faceset_id, string face_id)
        {
            string sendUrl = ADD_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_id=" + faceset_id + "&face_id=" + face_id;
            return added(sendUrl);
        }

        public IAddedState Added_FromId(string faceset_id, string[] face_ids)
        {
            return Added_FromId(faceset_id, String.Join(",", face_ids));
        }




        private IRemovedState removed(string sendUrl)
        {
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IRemovedState state = new RemovedState();
                state.CopyObject(obj as Dictionary<string, object>);
                return state;
            }
            catch (Exception)
            {
                
              //  throw;
            }
            return null;

        }
        IRemovedState Removed_FromName(string faceset_name, string face_id)
        {
            string sendUrl = REMOVE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_name=" + faceset_name + "&face_id=" + face_id;
            return removed(sendUrl);
        }

        public IRemovedState Removed_FromName(string faceset_name, string[] face_ids)
        {
            return Removed_FromName(faceset_name, String.Join(",", face_ids));
        }

        IRemovedState Removed_FromId(string faceset_id, string face_id)
        {
            string sendUrl = REMOVE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_id=" + faceset_id + "&face_id=" + face_id;
            return removed(sendUrl);
        }
        public IRemovedState Removed_FromName(string faceset_name)
        {
            string sendUrl = REMOVE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_name=" + faceset_name + "&face_id=all";
            return removed(sendUrl);
        }

        public IRemovedState Removed_FromId(string faceset_id)
        {
            string sendUrl = REMOVE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_id=" + faceset_id + "&face_id=all";
            return removed(sendUrl);
        }
        public IRemovedState Removed_FromId(string faceset_id, string[] face_ids)
        {
            return Removed_FromId(faceset_id, String.Join(",", face_ids));
        }

        //


        private IFaceset_getInfo set_info(string sendUrl)
        {
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IFaceset_getInfo b = new Faceset();
             //   b.CopyObject(obj as Dictionary<string, object>);
                return b;
            }
            catch (Exception)
            {

                //  throw;
            }
            return null;
        }

        public IFaceset_getInfo SetInfo_FromId(string faceset_id, string name = "", string tag = "")
        {
            string sendUrl = SETINFO_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_id=" + faceset_id;
            if (name != "") sendUrl += "&name=" + name;
            if (tag != "") sendUrl += "&tag=" + tag;
            return set_info(sendUrl);       
        }

        public IFaceset_getInfo SetInfo_FromName(string faceset_name, string name = "", string tag = "")
        {
            string sendUrl = SETINFO_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&faceset_name=" + faceset_name;
            if (name != "") sendUrl += "&name=" + name;
            if (tag != "") sendUrl += "&tag=" + tag;
            return set_info(sendUrl);       
        }




        //private IFacesetGetInfoed get_info(string sendUrl)
        //{
        //    try
        //    {
        //        string json = HttpHelper.CreateGetHttpText(sendUrl);
        //        object obj = Json.Deserialize(json);
        //        IFacesetGetInfoed info = new Faceset();
        //       // info.CopyObject(obj as Dictionary<string, object>);
        //        return info;
        //    }
        //    catch (Exception)
        //    {
                
        //      //  throw;
        //    }
        //    return null;
        //}

        //public IFacesetGetInfoed GetInfo_FromId(string faceset_id)
        //{
        //    string sendUrl = GETINFO_URL  + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
        //    sendUrl += "&faceset_id=" + faceset_id;
        //    return get_info(sendUrl);
        //}

        //public IFacesetGetInfoed GetInfo_FromName(string faceset_name)
        //{
        //    string sendUrl = GETINFO_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
        //    sendUrl += "&faceset_name=" + faceset_name;
        //    return get_info(sendUrl);
        //}


        public IFaceset_setInfo GetInfo_FromId(string faceset_id)
        {
            throw new NotImplementedException();
        }
    }
}
