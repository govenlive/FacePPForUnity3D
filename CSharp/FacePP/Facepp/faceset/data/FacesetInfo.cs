﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP.FacesetInfo
{
  

    public interface IFace
    {
        string Face_id { get; }

        string Tag { get; }

          void CopyObjectFromFacesetInfo(Dictionary<string, object> obj);

    }

    public interface IFaceset_getInfo
    {
        IFace[] Faces { get; }
        string Faceset_id { get; }
        string Faceset_name { get; }
        string Tag { get; }

        void CopyObjectFromFaceset_getInfo(Dictionary<string, object> obj);

      
    }

    public interface IFacesetCreated
    {
        string Faceset_id { get; }
        string Faceset_name { get; }
        string Tag { get; }
        /// <summary>
        /// 成功加入的face数量
        /// </summary>
        int Added_face { get; }
        void CopyObjectFromCreated(Dictionary<string, object> obj);
    }


    public interface IFaceset_setInfo
    {
        string Faceset_id { get; }
        string Tag { get; }
        /// <summary>
        /// 
        /// </summary>
        string Faceset_name { get; }

        void CopyObjectFromFaceSet_SetInfo(IDictionary<string, object> obj);

    }






}
