﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiniJSON;



namespace FacePP
{
    public interface IGroupManager
    {
        IGroupCreated Create_InfoFromId(string[] person_ids, string group_name = "", string tag = "");
        IGroupCreated Create_InfoFromName(string[] person_names, string group_name = "", string tag = "");
        IDeleteState Detele_FromName(string[] group_names);
        IDeleteState Detele_FromId(string[] group_ids);
        IAddedState AddPersonIds_toName(string group_name, string[] person_ids);
        IAddedState AddPersonNames_toName(string group_name, string[] person_names);
        IAddedState AddPersonIds_toId(string group_id, string[] person_ids);
        IAddedState AddPersonNames_toId(string group_id, string[] person_names);


        IRemovedState RemoveIds_FromName(string group_name, string[] person_ids);
        IRemovedState RemoveIds_FromName(string group_name);

        IRemovedState RemoveIds_FromId(string group_id, string[] person_ids);
        IRemovedState RemoveIds_FromId(string group_id);

        IRemovedState RemoveNames_FromName(string group_name, string[] person_names);
        IRemovedState RemoveNames_FromName(string group_name);

        IRemovedState RemoveNames_FromId(string group_id, string[] person_names);
        IRemovedState RemoveNames_FromId(string group_id);


        IGroup SetInfo_FromId(string group_id, string name = "", string tag = "");
        IGroup SetInfo_FromName(string group_name, string name = "", string tag = "");

        IGroupGetInfoed GetInfo_FromId(string group_id);
        IGroupGetInfoed GetInfo_FromName(string group_name);
    }
    public class GroupManager:IGroupManager
    {

        private const string CREATE_URL = "https://apicn.faceplusplus.com/v2/group/create";
        private const string DELETE_URL = "https://apicn.faceplusplus.com/v2/group/delete";
        private const string ADD_URL = "https://apicn.faceplusplus.com/v2/group/add_face";
        private const string REMOVE_URL = "https://apicn.faceplusplus.com/v2/group/remove_face";
        private const string SETINFO_URL = "https://apicn.faceplusplus.com/v2/group/set_info";
        private const string GETINFO_URL = "https://apicn.faceplusplus.com/v2/group/get_info";


        IGroupCreated Create_InfoFromId(string person_id , string group_name , string tag = "")
        {
            string sendUrl = getSendUrl(group_name, tag);          
            sendUrl += "&person_id=" + person_id;          
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IGroupCreated info = new GroupCreated();
                info.CopyObject(obj as Dictionary<string, object>);
                return info;
            }
            catch (Exception)
            {
                // throw;
            }
            return null;
        }
        string getSendUrl(string group_name, string tag)
        {
           return CREATE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret+ "&group_name=" + group_name+"&tag=" + tag;
        }
        IGroupCreated Create_InfoFromName(string person_name, string group_name, string tag = "")
        {
            string sendUrl = getSendUrl(group_name, tag);
            sendUrl += "&person_name=" + person_name;
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IGroupCreated info = new GroupCreated();
                info.CopyObject(obj as Dictionary<string, object>);
                return info;
            }
            catch (Exception)
            {
                // throw;
            }
            return null;
        }



        public IGroupCreated Create_InfoFromId(string[] person_ids, string group_name = "", string tag = "")
        {
            return Create_InfoFromId(String.Join(",", person_ids),group_name, tag);
        }

        public IGroupCreated Create_InfoFromName(string[] person_names, string group_name = "", string tag = "")
        {
            return Create_InfoFromName(String.Join(",", person_names), group_name, tag);
        }



        private IDeleteState detele(string sendUrl)
        {
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IDeleteState state = new DeleteState();
                state.CopyObject(obj as Dictionary<string, object>);
                return state;
            }
            catch (Exception)
            {

                // throw;
            }
            return null;
        }

        IDeleteState Detele_FromName(string group_name)
        {
            string sendUrl = DELETE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_name=" + group_name;
            return detele(sendUrl);
        }
        IDeleteState Detele_FromId(string group_id)
        {
            string sendUrl = DELETE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_id=" + group_id;
            return detele(sendUrl);
        }

        public IDeleteState Detele_FromName(string[] group_names)
        {
            return Detele_FromName(String.Join(",", group_names ));
        }

        public IDeleteState Detele_FromId(string[] group_ids)
        {
            return Detele_FromId(String.Join(",", group_ids));
        }


        private IAddedState added(string sendUrl)
        {
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IAddedState state = new AddedState();
                state.CopyObject(obj as Dictionary<string, object>);
                return state;
            }
            catch (Exception)
            {

                //  throw;
            }
            return null;
        }
        IAddedState AddPersonIds_toName(string group_name, string person_id)
        {
            string sendUrl = ADD_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_name=" + group_name + "&person_id=" + person_id;
            return added(sendUrl);
        }

        IAddedState AddPersonIds_toId(string group_id, string person_id)
        {
            string sendUrl = ADD_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_id=" + group_id + "&person_id=" + person_id;
            return added(sendUrl);
        }

        IAddedState AddPersonNames_toName(string group_name, string person_name)
        {
            string sendUrl = ADD_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_name=" + group_name + "&person_name=" + person_name;
            return added(sendUrl);
        }

        IAddedState AddPersonNames_toId(string group_id, string person_name)
        {
            string sendUrl = ADD_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_id=" + group_id + "&person_name=" + person_name;
            return added(sendUrl);
        }


        public IAddedState AddPersonIds_toName(string group_name, string[] person_ids)
        {
            return AddPersonIds_toName(group_name, string.Join(",", person_ids));
        }

        public IAddedState AddPersonNames_toName(string group_name, string[] person_names)
        {
            return AddPersonNames_toName(group_name, string.Join(",", person_names));
        }

        public IAddedState AddPersonIds_toId(string group_id, string[] person_ids)
        {
            return AddPersonIds_toId(group_id, string.Join(",", person_ids));
        }

        public IAddedState AddPersonNames_toId(string group_id, string[] person_names)
        {
            return AddPersonNames_toId(group_id, string.Join(",", person_names));
        }


        //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>///
        private IRemovedState removed(string sendUrl)
        {
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IRemovedState state = new RemovedState();
                state.CopyObject(obj as Dictionary<string, object>);
                return state;
            }
            catch (Exception)
            {

                //  throw;
            }
            return null;

        }
        IRemovedState RemoveIds_FromName(string group_name, string person_id)
        {
            string sendUrl = REMOVE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_name=" + group_name + "&person_id=" + person_id;
            return removed(sendUrl);
        }
        public IRemovedState RemoveIds_FromName(string group_name, string[] person_ids)
        {
            return RemoveIds_FromName(group_name, string.Join(",", person_ids));
        }
        public IRemovedState RemoveIds_FromName(string group_name)
        {
            return RemoveIds_FromName(group_name, "all");
        }

        IRemovedState RemoveIds_FromId(string group_id, string person_id)
        {
            string sendUrl = REMOVE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_id=" + group_id + "&person_id=" + person_id;
            return removed(sendUrl);
        }

        public IRemovedState RemoveIds_FromId(string group_id, string[] person_ids)
        {
            return RemoveIds_FromId(group_id, string.Join(",", person_ids));
        }

        public IRemovedState RemoveIds_FromId(string group_id)
        {
            return RemoveIds_FromId(group_id,"all");
        }


        IRemovedState RemoveNames_FromName(string group_name, string person_name)
        {
            string sendUrl = REMOVE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_name=" + group_name + "&person_name=" + person_name;
            return removed(sendUrl);
        }

        public IRemovedState RemoveNames_FromName(string group_name, string[] person_names)
        {
            return RemoveNames_FromName(group_name, string.Join(",", person_names));
        }

        public IRemovedState RemoveNames_FromName(string group_name)
        {
            return RemoveNames_FromName(group_name,"all");
        }


        IRemovedState RemoveNames_FromId(string group_id, string person_name)
        {
            string sendUrl = REMOVE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_id=" + group_id + "&person_name=" + person_name;
            return removed(sendUrl);
        }

        public IRemovedState RemoveNames_FromId(string group_id, string[] person_names)
        {
            return RemoveNames_FromId(group_id, string.Join(",", person_names));
        }

        public IRemovedState RemoveNames_FromId(string group_id)
        {
            return RemoveNames_FromId(group_id, "all");
        }


        private IGroup set_info(string sendUrl)
        {
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IGroup b = new Group();
                b.CopyObject(obj as Dictionary<string, object>);
                return b;
            }
            catch (Exception)
            {

                //  throw;
            }
            return null;
        }
        public IGroup SetInfo_FromId(string group_id, string name = "", string tag = "")
        {
            string sendUrl = SETINFO_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_id=" + group_id;
            if (name != "") sendUrl += "&name=" + name;
            if (tag != "") sendUrl += "&tag=" + tag;
            return set_info(sendUrl);  
        }

        public IGroup SetInfo_FromName(string group_name, string name = "", string tag = "")
        {
            string sendUrl = SETINFO_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_name=" + group_name;
            if (name != "") sendUrl += "&name=" + name;
            if (tag != "") sendUrl += "&tag=" + tag;
            return set_info(sendUrl);   
        }


        private IGroupGetInfoed get_info(string sendUrl)
        {
            try
            {
                string json = HttpHelper.CreateGetHttpText(sendUrl);
                object obj = Json.Deserialize(json);
                IGroupGetInfoed info = new GroupGetInfoed();
                info.CopyObject(obj as Dictionary<string, object>);
                return info;
            }
            catch (Exception)
            {

                //  throw;
            }
            return null;
        }


        public IGroupGetInfoed GetInfo_FromId(string group_id)
        {
            string sendUrl = GETINFO_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_id=" + group_id;
            return get_info(sendUrl);
        }

        public IGroupGetInfoed GetInfo_FromName(string group_name)
        {
            string sendUrl = GETINFO_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            sendUrl += "&group_name=" + group_name;
            return get_info(sendUrl);
        }
    }
}
