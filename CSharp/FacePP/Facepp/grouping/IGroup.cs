﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP.Grouping
{
    public interface IFace
    {
        string Face_id { get; }
        string Tag { get; }

        void CopyObjectFromGrouping(Dictionary<string, object> obj);

    }
   
    public interface IGroup
    {
        IFace[] Faces { get; }
        void CopyObject(Dictionary<string, object> obj);
    }
    public class Group:IGroup
    {
        private IFace[] faces;
        public IFace[] Faces
        {
            get { return faces; }
        }

        public void CopyObject(Dictionary<string, object> obj)
        {
            IList<object> arr = obj["face"] as List<object>;
            faces = new IFace[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                IFace face = new Face();
                face.CopyObjectFromGrouping(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }
        }
    }

    public interface IResult
    {
        /// <summary>
        /// 已经归类好的
        /// </summary>
        IGroup[] Groups { get; }
        /// <summary>
        /// 无法归类的
        /// </summary>
        IFace[] UnGroupeds { get; }
        void CopyObject(Dictionary<string, object> obj);
    }

    public class Result : IResult
    {
        private IGroup[] groups;
        private IFace[] ungroupeds;
        public IGroup[] Groups
        {
            get { return groups; }
        }

        public IFace[] UnGroupeds
        {
            get { return ungroupeds; }
        }

        public void CopyObject(Dictionary<string, object> obj)
        {
            int i = 0;
            List<object> arr = obj["group"] as List<object>;
            groups = new IGroup[arr.Count];
            for(i=0;i<arr.Count;i++){
                IGroup group = new Group();
                group.CopyObject(arr[i] as Dictionary<string, object>);
                groups[i] = group;
            }

            arr = obj["ungrouped"] as List<object>;
            ungroupeds = new IFace[arr.Count];
            for(i=0;i<arr.Count;i++)
            {
                IFace face = new Face();
                face.CopyObjectFromGrouping(arr[i] as Dictionary<string, object>);
                ungroupeds[i] = face;
            }
        } 
    }
    public interface ISession
    {
        /// <summary>
        /// 任务开始时间，单位：秒
        /// </summary>
        int Create_time { get; }
        /// <summary>
        /// 任务结束时间，单位：秒
        /// </summary>
        int Finish_time { get; }    
        /// <summary>
        /// 结果
        /// </summary>
        IResult Result { get; }
        /// <summary>
        /// 相应请求的session标识符，可用于结果查询
        /// </summary>
        string Session_id { get; }
        /// <summary>
        /// 取值为 SUCC(已完成) / FAILED(失败) / INQUEUE(队列中)
        /// </summary>
        StatuEnum Status { get; }
        void CopyObject(Dictionary<string, object> obj);
    }
}
