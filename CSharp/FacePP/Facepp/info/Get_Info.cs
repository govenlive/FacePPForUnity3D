﻿using System;
using System.Collections.Generic;
using MiniJSON;


namespace FacePP.Infos
{
    public interface IGet_info{
        /// <summary>
        /// 获取一张image的信息, 包括其中包含的face等信息
        /// </summary>
        /// <param name="img_id">目标图片的img_id</param>
        /// <returns></returns>
        IImageInfo GetImage(string img_id);
        /// <summary>
        /// 给定一组Face，返回相应的信息(包括源图片, 相关的person等等)。
        /// </summary>
        /// <param name="face_ids"></param>
        /// <returns></returns>
        IFaceInfo[] GetFace(string[] face_ids);

        /// <summary>
        /// 返回该App中的所有Person
        /// </summary>
        /// <returns></returns>
        IPerson[] Get_Person_List();
        /// <summary>
        /// 返回该App中的所有Faceset。
        /// </summary>
        /// <returns></returns>
        IFaceset[] Get_Faceset_List();

        /// <summary>
        /// 返回这个App中的所有Group
        /// </summary>
        /// <returns></returns>
        IGroup[] Get_Group_List();

        /// <summary>
        /// 获取session相关状态和结果
        /// (所有session都将在计算完成72小时之后过期，并被自动清除。)
        /// </summary>
        /// <param name="session_id"></param>
        /// <returns></returns>
        ISessionInfo Get_Session(string session_id);

        /// <summary>
        /// 获取该App相关的信息
        /// </summary>
        /// <returns></returns>
        IAppInfo Get_App();
    }


    public class Get_info:IGet_info
    {
        private const string GET_IMAGE_URL="https://apicn.faceplusplus.com/v2/info/get_image";
        private const string GET_FACEI_URL = "https://apicn.faceplusplus.com/v2/info/get_face";
        private const string GET_PERSON_LIST_URL = "https://apicn.faceplusplus.com/v2/info/get_person_list";
        private const string GET_FACESET_LIST_URL = "https://apius.faceplusplus.com/v2/info/get_faceset_list";
        private const string GET_GROUP_LIST_URL = "https://apicn.faceplusplus.com/v2/info/get_group_list";
        private const string GET_SESSION_URL = "https://apicn.faceplusplus.com/v2/info/get_session";
        private const string GET_APP_URL = "https://apicn.faceplusplus.com/v2/info/get_app";

        public IImageInfo GetImage(string img_id)
        {
            string sendUrl = GET_IMAGE_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret + "&img_id=" + img_id;
            try
            {
                string jsonStr = HttpHelper.CreateGetHttpText(sendUrl);
                IImageInfo info = new ImageInfo();
                info.CopyObject(Json.Deserialize(jsonStr) as Dictionary<string, object>);
                return info;
            }
            catch (Exception)
            {
                
               
            }
            return null;
        }

        public IFaceInfo[] GetFace(string[] face_ids)
        {
            string face_id = string.Join(",", face_ids);
            string sendUrl = GET_FACEI_URL  + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret + "&face_id=" +face_id;

            try
            {
                string jsonStr = HttpHelper.CreateGetHttpText(sendUrl);
                IDictionary<string,object> obj = Json.Deserialize(jsonStr) as Dictionary<string, object>;
                IList<object> arr = obj["face_info"] as List<object>;
                IFaceInfo[] infos = new IFaceInfo[arr.Count];
                for(int i=0;i<arr.Count;i++)
                {
                    IFaceInfo info = new FaceInfo();
                  //  info.CopyObject(arr[i] as Dictionary<string, object>);
                    infos[i] = info;
                }
                return infos;
            }
            catch (Exception)
            {


            }
            return null;
        }


        public IPerson[] Get_Person_List()
        {
            string sendUrl = GET_PERSON_LIST_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            try
            {
                string jsonStr = HttpHelper.CreateGetHttpText(sendUrl);
                IDictionary<string, object> obj = Json.Deserialize(jsonStr) as Dictionary<string, object>;
                IList<object> arr = obj["person"] as List<object>;
                IPerson[] persons = new IPerson[arr.Count];
                for (int i = 0; i < arr.Count; i++)
                {
                    IPerson person = new Person();
                    person.CopyObject(arr[i] as Dictionary<string, object>);
                    persons[i] = person;
                }
                return persons;

            }
            catch (Exception)
            {


            }
            return null;



        }


        public IFaceset[] Get_Faceset_List()
        {
            string sendUrl = GET_FACESET_LIST_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            try
            {
                string jsonStr = HttpHelper.CreateGetHttpText(sendUrl);
                IDictionary<string, object> obj = Json.Deserialize(jsonStr) as Dictionary<string, object>;
                IList<object> arr = obj["faceset"] as List<object>;
                IFaceset[] facesets = new IFaceset[arr.Count];

                for (int i = 0; i < arr.Count; i++)
                {
                    IFaceset faceset = new Faceset();
                //    faceset.CopyObject(arr[i] as Dictionary<string, object>);
                    facesets[i] = faceset;
                }
                return facesets;

            }
            catch (Exception)
            {


            }
            return null;
        }




        public IGroup[] Get_Group_List()
        {
            string sendUrl = GET_GROUP_LIST_URL  + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            try
            {
                string jsonStr = HttpHelper.CreateGetHttpText(sendUrl);
                IDictionary<string, object> obj = Json.Deserialize(jsonStr) as Dictionary<string, object>;
                IList<object> arr = obj["group"] as List<object>;
                IGroup[] groups = new IGroup[arr.Count];
                for (int i = 0; i < arr.Count; i++)
                {
                    IGroup group = new Group();
                    group.CopyObject(arr[i] as Dictionary<string, object>);
                    groups[i] = group;
                }
                return groups;

            }
            catch (Exception)
            {


            }
            return null;
        }


        public ISessionInfo Get_Session(string session_id)
        {
            string sendUrl = GET_SESSION_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret + "&session_id=" + session_id;

            try
            {
                string jsonStr = HttpHelper.CreateGetHttpText(sendUrl);
                IDictionary<string, object> obj = Json.Deserialize(jsonStr) as Dictionary<string, object>;
                ISessionInfo info = new SessionInfo();
                info.CopyObject(obj);
                return info;
            }
            catch (Exception)
            {


            }
            return null;
        }


        public IAppInfo Get_App()
        {
            string sendUrl = GET_APP_URL + "?api_key=" + Facepp_config.api_key + "&api_secret=" + Facepp_config.api_secret;
            UnityEngine.Debug.Log(sendUrl);
         
            try
            {

                string jsonStr = HttpHelper.CreateGetHttpText(sendUrl);
           
                IDictionary<string, object> obj = Json.Deserialize(jsonStr) as Dictionary<string, object>;
                IAppInfo info = new AppInfo();
                info.CopyObject(obj);
                return info;
            }
            catch (Exception)
            {


            }
            return null;
        }
    }
}
