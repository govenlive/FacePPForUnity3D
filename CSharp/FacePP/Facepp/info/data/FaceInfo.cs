﻿using System;
using System.Collections.Generic;

namespace FacePP.Infos
{
    public interface IFaceset
    {
        string Faceset_id { get; }
        string Faceset_name { get; }
        string Tag { get; }
        void CopyFromGet_face(Dictionary<string, object> obj);
    }
     
    public interface IFaceInfo
    {
      //  Infos.IFace
        Attribute Attribute { get; }
        Position Position { get; }
        string Face_id { get; }
        string Tag { get; }
        string Url { get; }

        string Img_id { get; }

        IFaceset[] Facesets { get; }
        IPerson[] Persons { get; }

        void CopyFromGet_face(IDictionary<string, object> obj);

    }


    public class FaceInfo : IFaceInfo
    {
        private Attribute attribute;
        private Position position;
        private string face_id;
        private string tag;
        private string url;
        private string img_id;

        private IDictionary<int, IFaceset> facesets;
        private IDictionary<int, IPerson> persons;



        public Position Position
        {
            get { return position; }
        }

        public string Face_id
        {
            get { return face_id; }
        }

        public string Tag
        {
            get { return tag; }
        }

        public string Url
        {
            get { return url; }
        }

        public IDictionary<int, IFaceset> Facesets
        {
            get { return facesets; }
        }

        public IDictionary<int, IPerson> Persons
        {
            get { return persons; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            attribute = new Attribute();
            attribute.copyObject(obj["attribute"] as Dictionary<string, object>);
            face_id = obj["face_id"].ToString();

            int i;
            facesets = new Dictionary<int, IFaceset>();
            IList<object> arr = obj["faceset"] as List<object>;
            for (i = 0; i < arr.Count; i++)
            {
                IFaceset faceset = new Faceset();
             //   faceset.CopyObject(arr[i] as Dictionary<string, object>);
                facesets[i] = faceset;
            }
            img_id = obj["img_id"].ToString();

            persons = new Dictionary<int, IPerson>();
            arr = obj["person"] as List<object>;
            for (i = 0; i < arr.Count; i++)
            {
                IPerson person = new Person();
                person.CopyObject(arr[i] as Dictionary<string, object>);
                persons[i] = person;
            }

            position = new Position();
            position.copyObject(obj["position"] as Dictionary<string, object>);

            tag = obj["tag"].ToString();
            url = obj["url"].ToString();



        }

        public Attribute Attribute
        {
            get { return attribute; }
        }


        public string Img_id
        {
            get { throw new NotImplementedException(); }
        }


        IFaceset[] IFaceInfo.Facesets
        {
            get { throw new NotImplementedException(); }
        }

        IPerson[] IFaceInfo.Persons
        {
            get { throw new NotImplementedException(); }
        }

        public void CopyFromGet_face(IDictionary<string, object> obj)
        {
            throw new NotImplementedException();
        }
    }
}
