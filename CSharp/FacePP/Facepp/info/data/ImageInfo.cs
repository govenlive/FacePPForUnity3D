﻿using System.Collections.Generic;

namespace FacePP
{
    public interface IImageInfo
    {
        IDictionary<int, IFaceBasic> Faces { get; }
        string Img_id { get; }
        string Url { get; }

        void CopyObject(IDictionary<string, object> obj);
    }

 




    public class ImageInfo : IImageInfo
    {
        private IDictionary<int, IFaceBasic> faces;
        private string img_id;
        private string url;

        public IDictionary<int, IFaceBasic> Faces
        {
            get { return faces; }
        }

        public string Img_id
        {
            get { return img_id; }
        }

        public string Url
        {
            get { return url; }
        }


        public void CopyObject(IDictionary<string, object> obj)
        {
            faces = new Dictionary<int, IFaceBasic>();
            IList<object> arr = obj["face"] as List<object>;
            for (int i = 0; i < arr.Count; i++)
            {
                IFaceBasic face = new Face();
                face.CopyObject(arr[i] as Dictionary<string, object>);
            }

            img_id = obj["img_id"].ToString();
            url = obj["url"].ToString();
        }
    }
}
