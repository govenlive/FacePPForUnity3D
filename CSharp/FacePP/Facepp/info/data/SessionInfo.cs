﻿using System.Collections.Generic;
namespace FacePP
{
    /// <summary>
    /// 队列状态
    /// </summary>
    public enum StatuEnum
    {
        /// <summary>
        /// 队列中
        /// </summary>
        INQUEUE,
        /// <summary>
        /// 成功
        /// </summary>
        SUCC,
        /// <summary>
        /// 失败
        /// </summary>
        FAILED
    }
}

namespace FacePP.Infos
{
    #region Face
    
  
    /// <summary>
    /// 器官位置
    /// </summary>
    public enum OrganEnum
    {
        EYE_LEFT,
        EYE_RIGHT,
        CENTER,
        MOUTH_LEFT,
        MOUTH_RIGHT       
    }
    /// <summary>
    /// 脸部信息
    /// </summary>
    public interface IFace
    {
        /// <summary>
        /// 图片大小
        /// </summary>
        float Width { get; }
        /// <summary>
        /// 图片大小
        /// </summary>
        float Height { get; }

        string Face_id { get; }
        /// <summary>
        /// 器官集合
        /// </summary>
        IDictionary<OrganEnum, OrganPosition> Organs { get; }
        /// <summary>
        /// 一些属性
        /// </summary>
        Attribute Attribute { get; }

        void CopyObject (IDictionary<string,object> obj);
    }

    /// <summary>
    /// 脸部信息
    /// </summary>
    public class Face : IFace
    {
        private float width;
        private float height;
        private string face_id;
        private IDictionary<OrganEnum, OrganPosition> organs;
        private Attribute attribute;
        public float Width
        {
            get { return width; }
        }

        public float Height
        {
            get { return height; }
        }

        public string Face_id
        {
            get { return face_id; }
        }

        public IDictionary<OrganEnum, OrganPosition> Organs
        {
            get { return organs; }
        }

        public Attribute Attribute
        {
            get { return attribute; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            width = float.Parse(obj["width"].ToString());
            height = float.Parse(obj["height"].ToString());
            face_id = obj["face_id"].ToString();
            attribute = new Attribute();
            attribute.copyObject(obj["attribute"] as IDictionary<string,object>);
            organs = new Dictionary<OrganEnum, OrganPosition>();

            //添加一些器官~~~
            addOrgnInArr(OrganEnum.CENTER, obj["center"] as Dictionary<string, object>);
            addOrgnInArr(OrganEnum.EYE_LEFT, obj["eye_left"] as Dictionary<string, object>);
            addOrgnInArr(OrganEnum.EYE_RIGHT, obj["eye_right"] as Dictionary<string, object>);
            addOrgnInArr(OrganEnum.MOUTH_LEFT, obj["mouth_left"] as Dictionary<string, object>);
            addOrgnInArr(OrganEnum.MOUTH_RIGHT, obj["mouth_right"] as Dictionary<string, object>);
      

        }

        /// <summary>
        /// 将器官加入数组
        /// </summary>
        /// <param name="enumType"></param>
        /// <param name="obj"></param>
        private void addOrgnInArr(OrganEnum enumType, Dictionary<string, object> obj)
        {
            OrganPosition orgn = new OrganPosition();
            orgn.copyObject(obj);
            organs[enumType] = orgn;
        }
    }
    #endregion

    #region result
    
  
    public interface IResult
    {
        /// <summary>
        /// 图片链接
        /// </summary>
        string Url { get; }
        /// <summary>
        /// 图片id
        /// </summary>
        string Img_id { get; }
        /// <summary>
        /// 图片大小
        /// </summary>
        float Img_width { get; }
        /// <summary>
        /// 图片大小
        /// </summary>
        float Img_height { get; }
      
        /// <summary>
        /// 脸部集合
        /// </summary>
        IFace[] Faces { get; }

        void CopyObject(IDictionary<string, object> obj);
    }

    public class Result : IResult
    {
        private string url;
        private string img_id;
        private float img_width;
        private float img_height;
        private IFace[] faces;

        public string Url
        {
            get { return url; }
        }

        public string Img_id
        {
            get { return img_id; }
        }

        public float Img_width
        {
            get { return img_width; }
        }

        public float Img_height
        {
            get { return img_height; }
        }

        public IFace[] Faces
        {
            get { return faces; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            url = obj["url"].ToString();
            img_height = float.Parse(obj["img_height"].ToString());
            img_width = float.Parse(obj["img_width"].ToString());
            img_id = obj["img_id"].ToString();
            IList<object> arr = obj["face"] as List<object>;
            faces = new IFace[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                IFace face = new Face();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }
        }
    }



    #endregion

    #region ression

   



    public interface ISessionInfo
    {
        /// <summary>
        /// 任务开始时间，单位：秒
        /// </summary>
        int Create_time { get; }
        /// <summary>
        /// 任务结束时间，单位：秒
        /// </summary>
        int Finish_time { get; }
        /// <summary>
        /// 相应请求的session标识符，可用于结果查询
        /// </summary>
        string Session_id { get; }
        /// <summary>
        /// 可能取值有：INQUEUE(队列中), SUCC(成功) 和FAILED(失败)
        /// </summary>
        StatuEnum Status { get; }
        /// <summary>
        /// 返回session_id对应的结果内容
        /// </summary>
        IResult Result { get; }

        void CopyObject(IDictionary<string, object> obj);
    }
   
    public class SessionInfo : ISessionInfo
    {
        private int create_time;
        private int finish_time;
        private string session_id;
        private StatuEnum status;
        private IResult result;

        public int Create_time
        {
            get { return create_time; }
        }

        public int Finish_time
        {
            get { return create_time; }
        }

        public string Session_id
        {
            get { return session_id; }
        }

        public StatuEnum Status
        {
            get { return status; }
        }

        public IResult Result
        {
            get { return result; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            string statusStr = obj["status"].ToString();
            switch (statusStr)
            {
                case "INQUEUE":
                    status = StatuEnum.INQUEUE;
                    break;

                case "SUCC":
                    status = StatuEnum.SUCC;
                    break;

                case "FAILED":
                    status = StatuEnum.FAILED;
                    break;
                default:
                    break;
            }

            if(status==StatuEnum.SUCC)
            {
                create_time = int.Parse(obj["create_time"].ToString());
                finish_time = int.Parse(obj["finish_time"].ToString());
                session_id = obj["session_id"].ToString();

                result = new Result();
                result.CopyObject(obj["result"] as Dictionary<string, object>);
            }
            

           
        }


    }
    #endregion

}
