﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MiniJSON;

namespace FacePP.Recognition.Search
{
    public interface IFace_search
    {
        /// <summary>
        /// 给定一个Face和一个Faceset，在该Faceset内搜索最相似的Face。提示：若搜索集合需要包含超过10000张人脸，可以分成多个faceset分别调用search功能再将结果按confidence顺序合并即可。
        /// </summary>
        /// <param name="face_id">待搜索的Face的face_id</param>
        /// <param name="faceset_id">指定搜索范围为此Faceset</param>
        /// <param name="count">表示一共获取不超过count个搜索结果。默认count=3</param>
        /// <param name="async">如果置为true，该API将会以异步方式被调用；也就是立即返回一个session id，稍后可通过/info/get_session查询结果。默认值为false。</param>
        /// <returns></returns>
        ISearch SearchFromFacesetId(string face_id, string faceset_id, int count = 3, bool async = false);
        /// <summary>
        /// 给定一个Face和一个Faceset，在该Faceset内搜索最相似的Face。提示：若搜索集合需要包含超过10000张人脸，可以分成多个faceset分别调用search功能再将结果按confidence顺序合并即可。
        /// </summary>
        /// <param name="face_id">待搜索的Face的face_id</param>
        /// <param name="faceset_name">指定搜索范围为此Faceset</param>
        /// <param name="count">表示一共获取不超过count个搜索结果。默认count=3</param>
        /// <param name="async">如果置为true，该API将会以异步方式被调用；也就是立即返回一个session id，稍后可通过/info/get_session查询结果。默认值为false。</param>
        /// <returns></returns>
        ISearch SearchFromFacesetName(string face_id, string faceset_name, int count = 3, bool async = false);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="jsonStr"></param>
        /// <returns></returns>
        ISearch SearchFromJsonStr(string jsonStr);
    }
    public class Face_search:IFace_search
    {
        private const string API_URL="https://apicn.faceplusplus.com/v2/recognition/search";
        public ISearch SearchFromFacesetId(string face_id, string faceset_id, int count = 3, bool async = false)
        {
            string sendUrl = API_URL + "?api_secret=" + Facepp_config.api_secret + "&api_key=" + Facepp_config.api_key + "&face_id=" + face_id + "&faceset_id=" + faceset_id + "&count=" + count.ToString() + "&async=" + async;
            return SearchFromJsonStr(HttpHelper.CreateGetHttpText(sendUrl));
        }

        public ISearch SearchFromFacesetName(string face_id, string faceset_name, int count = 3, bool async = false)
        {
            string sendUrl = API_URL + "?api_secret=" + Facepp_config.api_secret + "&api_key=" + Facepp_config.api_key + "&face_id=" + face_id + "&faceset_name=" + faceset_name + "&count=" + count.ToString() + "&async=" + async;
            return SearchFromJsonStr(HttpHelper.CreateGetHttpText(sendUrl));
        }

        public ISearch SearchFromJsonStr(string jsonStr)
        {
            try
            {
                ISearch search = new Search();
                search.CopyObject(Json.Deserialize(jsonStr) as Dictionary<string, object>);
                return search;
            }
            catch (Exception)
            {
                
                //throw;
            }
            return null;
        }
    }
}
