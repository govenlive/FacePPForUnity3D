﻿using System;
using System.Collections.Generic;
using MiniJSON;

namespace FacePP.Recognition
{
    public interface IFace_verify
    {
        /// <summary>
        /// 给定一个Face和一个Person，返回是否是同一个人的判断以及置信度。
        /// </summary>
        /// <param name="face_id">待verify的face_id</param>
        /// <param name="person_id">对应的Person</param>
        /// <param name="async">如果置为true，该API将会以异步方式被调用；也就是立即返回一个session id，稍后可通过/info/get_session查询结果。默认值为false。</param>
        /// <returns></returns>
        IVerify VerifyTheFaceIdFromPersonId(string face_id, string person_id,bool async=true);
        /// <summary>
        /// 给定一个Face和一个Person，返回是否是同一个人的判断以及置信度。
        /// </summary>
        /// <param name="face_id">待verify的face_id</param>
        /// <param name="person_name">对应的Person</param>
        /// <param name="async">如果置为true，该API将会以异步方式被调用；也就是立即返回一个session id，稍后可通过/info/get_session查询结果。默认值为false。</param>
        /// <returns></returns>
        IVerify VerifyTheFaceIdFromPersonName(string face_id, string person_name, bool async=true);
        IVerify VerifyTheFaceIdFromJsonStr(string jsonStr);
    }
    public class Face_verify : IFace_verify
    {
        private static IFace_verify instance;
        /// <summary>
        /// 单例模式
        /// </summary>
        public static IFace_verify Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Face_verify();
                }
                return instance;
            }
        }




        private const string API_URL = "https://apicn.faceplusplus.com/v2/recognition/verify";


        public IVerify VerifyTheFaceIdFromPersonId(string face_id, string person_id, bool async = true)
        {
            string sendUrl = API_URL + "?api_secret=" + Facepp_config.api_secret + "&api_key=" + Facepp_config.api_key + "&face_id=" + face_id + "&person_id=" + person_id + "&async=" + async.ToString();
            return VerifyTheFaceIdFromJsonStr(HttpHelper.CreateGetHttpText(sendUrl));
        }

        public IVerify VerifyTheFaceIdFromPersonName(string face_id, string person_name, bool async = true)
        {
            string sendUrl = API_URL + "?api_secret=" + Facepp_config.api_secret + "&api_key=" + Facepp_config.api_key + "&face_id=" + face_id + "&person_name=" + person_name + "&async=" + async.ToString();
            return VerifyTheFaceIdFromJsonStr(HttpHelper.CreateGetHttpText(sendUrl));
        }

        public IVerify VerifyTheFaceIdFromJsonStr(string jsonStr)
        {
            try
            {
                IVerify verify = new Verify();
                verify.CopyObject(Json.Deserialize(jsonStr) as Dictionary<string, object>);
                return verify;
            }
            catch (Exception)
            {
                
             //   throw;
            }
            return null;
        }
    }
}
