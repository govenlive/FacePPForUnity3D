﻿using System;
using System.Collections.Generic;
using MiniJSON;

namespace FacePP.Recognition
{
    /// <summary>
    /// 计算两个Face的相似性以及五官相似度
    /// </summary>
    public interface ICompareManager
    {     
        /// <summary>
        /// 获取两个face_id的相似度
        /// </summary>
        /// <param name="face_id1">第一个Face的face_id</param>
        /// <param name="face_id2">第二个Face的face_id</param>
        /// <param name="async">如果置为true，该API将会以异步方式被调用；也就是立即返回一个session id，稍后可通过/info/get_session查询结果。默认值为false</param>
        /// <returns></returns>
        IFaceCompare CompareTwoFaceId(string face_id1, string face_id2, bool async=false);
       

        /// <summary>
        /// 通过拿到的json 获取相似度
        /// </summary>
        /// <param name="jsonStr"></param>
        /// <returns></returns>
        IFaceCompare CompareTwoFaceIdFromJsonString(string jsonStr);




    }


    public class CompareManager : ICompareManager
    {
        private static ICompareManager instance;
        /// <summary>
        /// 单例模式
        /// </summary>
        public static ICompareManager Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CompareManager();
                }
                return instance;
            }
        }


        private const string API_URL = "https://apicn.faceplusplus.com/v2/recognition/compare";
        public IFaceCompare CompareTwoFaceId(string face_id1, string face_id2, bool async = false)
        {
            string sendUrl = API_URL + "?api_secret=" + Facepp_config.api_secret + "&api_key=" + Facepp_config.api_key + "&face_id1=" + face_id1 + "&face_id2=" + face_id2;
            return CompareTwoFaceIdFromJsonString(HttpHelper.CreateGetHttpText(sendUrl));
        }

        public IFaceCompare CompareTwoFaceIdFromJsonString(string jsonStr)
        {
            try
            {
                IFaceCompare compare = new FaceCompare();
                compare.CopyObject(Json.Deserialize(jsonStr) as Dictionary<string, object>);
                return compare;
            }
            catch (Exception)
            {
                
                //throw;
            }
            return null;
        }
    }
    
}
