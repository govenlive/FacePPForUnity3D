﻿using System.Collections.Generic;

namespace FacePP.Recognition
{
   
  
   
    /// <summary>
    /// 五官组件相似度
    /// </summary>
    public interface IComponent_similarity
    {
        /// <summary>
        /// 眼睛相似度
        /// </summary>
        float Eye { get; }
        /// <summary>
        ///  嘴巴相似度
        /// </summary>
        float Mouth { get; }
        /// <summary>
        /// 鼻子相似度
        /// </summary>
        float Nose { get; }
        /// <summary>
        /// 眉毛相似度
        /// </summary>
        float Eyebrow { get; }

        void CopyObject(IDictionary<string, object> obj);
    }

    public class Component_similarity : IComponent_similarity
    {
        private float eye;
        private float mouth;
        private float nose;
        private float eyebrow;

        public float Eye
        {
            get { return eye; }
        }

        public float Mouth
        {
            get { return mouth; }
        }

        public float Nose
        {
            get { return nose; }
        }

        public float Eyebrow
        {
            get { return eyebrow; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            eye = float.Parse(obj["eye"].ToString());
            mouth = float.Parse(obj["mouth"].ToString());
            nose = float.Parse(obj["nose"].ToString());
            eyebrow = float.Parse(obj["eyebrow"].ToString());
        }
    }
    /// <summary>
    /// 计算两个Face的相似性以及五官相似度
    /// </summary>
    public interface IFaceCompare
    {
       
        float Similarity { get; }
        /// <summary>
        /// 一个0~100之间的实数，表示两个face的相似性
        /// </summary>
        string Session_id { get; }
        /// <summary>
        /// 包含人脸中各个部位的相似性，目前包含eyebrow(眉毛)、eye(眼睛)、nose(鼻子)与mouth（嘴）的相似性。每一项的值为一个0~100之间的实数，表示相应部位的相似性
        /// </summary>
        IComponent_similarity Component_similarity { get; }

        void CopyObject(IDictionary<string, object> obj);
    }

    public class FaceCompare:IFaceCompare
    {
        private float similarity;
        private string session_id;
        private IComponent_similarity component_similarity;


        public float Similarity
        {
            get { return similarity; }
        }

        public string Session_id
        {
            get { return session_id; }
        }

        public IComponent_similarity Component_similarity
        {
            get { return component_similarity; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            similarity = float.Parse(obj["similarity"].ToString());
            session_id = obj["session_id"].ToString();
            component_similarity = new Component_similarity();
            component_similarity.CopyObject(obj["component_similarity"] as Dictionary<string, object>);
        }
    } 
}
