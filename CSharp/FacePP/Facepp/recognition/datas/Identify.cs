﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FacePP.Recognition.Identify
{
    /// <summary>
    /// 对于一个待查询的Face列表（或者对于给定的Image中所有的Face），在一个Group中查询最相似的Person。
    /// </summary>
    public interface IIdentify
    {
        IIDentifyFace[] Faces { get; }

        string Session_id { get; }
        void CopyObject(IDictionary<string, object> obj);
    }


    public class Identify : IIdentify
    {
        private IIDentifyFace[] faces;
        private string session_id;
        public IIDentifyFace[] Faces
        {
            get { return faces; }
        }

        public string Session_id
        {
            get { return session_id; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            IList<object> arr = obj["face"] as List<object>;
            faces = new IIDentifyFace[arr.Count];
            for(int i=0;i<arr.Count;i++)
            {
                IIDentifyFace face = new IDentifyFace();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }
            session_id = obj["session_id"].ToString();
        }
    }

    public interface IIDentifyFace
    {
        ICandidate[] Candidates { get; }

        string Face_id { get; }

        Position Position { get; }
        void CopyObject(IDictionary<string, object> obj);
    }

    public class IDentifyFace : IIDentifyFace
    {
        private ICandidate[] candidates;
        private string face_id;
        private Position position;

        public ICandidate[] Candidates
        {
            get { return candidates; }
        }

        public string Face_id
        {
            get { return face_id; }
        }

        public Position Position
        {
            get { return position; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            IList<object> arr = obj["candidate"] as List<object>;
            candidates = new ICandidate[arr.Count];
            for(int i=0;i<arr.Count;i++)
            {
                ICandidate candidate = new Candidate();
                candidate.CopyObject(arr[i] as Dictionary<string, object>);
                candidates[i] = candidate;
            }

            face_id = obj["face_id"].ToString();
            position = new Position();
            position.copyObject(obj["position"] as Dictionary<string, object>);
        }
    }

    /// <summary>
    /// 识别结果包含相应person信息与相应的置信度
    /// </summary>
    public interface ICandidate
    {
        /// <summary>
        /// 置信度
        /// </summary>
        float Confidence { get; }
        string Person_id { get; }
        string Person_name { get; }
        string Tag { get; }
        void CopyObject(IDictionary<string, object> obj);
    }

    public class Candidate : ICandidate
    {
        private float confidence;
        private string persion_id;
        private string persion_name;
        private string tag;
        public float Confidence
        {
            get { return confidence; }
        }

        public string Person_id
        {
            get { return persion_id; }
        }

        public string Person_name
        {
            get { return persion_name; }
        }

        public string Tag
        {
            get { return tag; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            confidence = float.Parse(obj["confidence"].ToString());
            persion_id = obj["persion_id"].ToString();
            persion_name = obj["persion_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }
}