﻿using System.Collections.Generic;

namespace FacePP.Recognition.Search
{
    public interface ICandidate
    {
        /// <summary>
        /// face_id
        /// </summary>
        string Face_id { get; }
        /// <summary>
        /// 相似度
        /// </summary>
        float Similarity { get; }
        /// <summary>
        /// tag
        /// </summary>
        string Tag { get; }
        void CopyObject(IDictionary<string, object> obj);
    }
    public class Candidate : ICandidate
    {
        private string face_id;
        private float similarity;
        private string tag;
        public string Face_id
        {
            get { return face_id; }
        }

        public float Similarity
        {
            get { return similarity; }
        }

        public string Tag
        {
            get { return tag; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            face_id = obj["face_id"].ToString();
            similarity =float.Parse( obj["similarity"].ToString());
            tag = obj["obj"].ToString();
        }
    }
    public interface ISearch
    {
        ICandidate[] Candidates { get; }
        string Session_id { get; }
        void CopyObject(IDictionary<string, object> obj);

    }

    public class Search:ISearch
    {
        private ICandidate[] candidates;
        private string session_id;

        public ICandidate[] Candidates
        {
            get { return candidates; }
        }

        public string Session_id
        {
            get { return session_id; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            session_id = obj["session_id"].ToString();
            IList<object> arr = obj["candidate"] as List<object>;
            candidates = new ICandidate[arr.Count];
            for(int i=0;i<arr.Count;i++)
            {
                ICandidate candidate = new Candidate();
                candidate.CopyObject(arr[i] as Dictionary<string, object>);
                candidates[i] = candidate;
            }
        }
    }
}
