﻿using System.Collections.Generic;

namespace FacePP.Recognition
{
    /// <summary>
    /// 给定一个Face和一个Person，返回是否是同一个人的判断以及置信度 结果。
    /// </summary>
    public interface IVerify
    {
        /// <summary>
        /// 两个输入是否为同一人的判断
        /// </summary>
        bool Is_same_person { get; }
        /// <summary>
        /// 系统对这个判断的置信度。
        /// </summary>
        float Confidence { get; }
        /// <summary>
        /// 相应请求的session标识符，可用于结果查询
        /// </summary>
        string Session_id { get; }

        void CopyObject(IDictionary<string, object> obj);
    }

    public class Verify : IVerify
    {
        private bool is_same_person;
        private float confidence;
        private string session_id;
        public bool Is_same_person
        {
            get { return is_same_person; }
        }

        public float Confidence
        {
            get { return confidence; }
        }

        public string Session_id
        {
            get { return session_id; }
        }

        public void CopyObject(IDictionary<string, object> obj)
        {
            is_same_person = bool.Parse(obj["is_same_person"].ToString());
            confidence = float.Parse(obj["confidence"].ToString());
            session_id = obj["session_id"].ToString();
        }
    }
}
