﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{
    public interface IFaceService
    {
        T HttpPost<T>(string url,Dictionary<object, object> h, byte[] byt) where T : new();
        T HttpGet<T>(string url,Dictionary<object, object> h) where T : new();

    }
    public interface IResult
    {
        void CopyObject(Dictionary<string, object> obj);

    }

}
