﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{
    public struct FaceSetCreateResult : IResult
    {
        private string added_face;
        private string faceset_id;
        private string faceset_name;
        private string tag;

        public string Added_face { get { return added_face; } }
        public string Faceset_id { get { return faceset_id; } }
        public string Faceset_name { get { return faceset_name; } }
        public string Tag { get { return tag; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            added_face = obj["added_face"].ToString();
            faceset_id = obj["faceset_id"].ToString();
            faceset_name = obj["faceset_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }


    public struct FaceSetSetInfoResult:IResult
    {
        private string faceset_id;
        private string faceset_name;
        private string tag;
        public string Faceset_id { get { return faceset_id; } }
        public string Faceset_name { get { return faceset_name; } }
        public string Tag { get { return tag; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            faceset_id = obj["faceset_id"].ToString();
            faceset_name = obj["faceset_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }
}
