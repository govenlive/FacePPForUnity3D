﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{
    public class PersonBasicInfoNoNum
    {
        string person_id;
        string person_name;
        string tag;

        public string Person_id { get { return person_id; } }
        public string Person_name { get { return person_name; } }
        public string Tag { get; set; }

        public void CopyObject(Dictionary<string, object> obj)
        {
            person_id = obj["person_id"].ToString();
            person_name = obj["person_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }
    public class GroupBasicInfo:IResult
    {
        private int added_person;
        private string group_id;
        private string group_name;
        private string tag;

        public int Added_person { get { return added_person; } }
        public string Group_id { get { return group_id; } }
        public string Group_name { get { return group_name; } }
        public string Tag { get { return tag; } }

        public void CopyObject(Dictionary<string, object> obj)
        {

            added_person = int.Parse(obj["added_person"].ToString());
            group_id = obj["group_id"].ToString();
            group_name = obj["group_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }


    public class GroupInfo:IResult
    {
        private PersonBasicInfoNoNum[] persons;
        private string group_id;
        private string tag;
        private string group_name;


        public PersonBasicInfoNoNum[] Persons { get; set; }
        public string Group_id { get{return group_id;} }
        public string Tag { get { return tag; } }
        public string Group_name { get { return group_name; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            group_id = obj["group_id"].ToString();
            group_name = obj["group_name"].ToString();
            tag = obj["tag"].ToString();
            IList<object> arr = obj["person"] as List<object>;
            persons = new PersonBasicInfoNoNum[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                PersonBasicInfoNoNum person = new PersonBasicInfoNoNum();
                person.CopyObject(arr[i] as Dictionary<string, object>);
                persons[i] = person;
            }
        }
    }
}
