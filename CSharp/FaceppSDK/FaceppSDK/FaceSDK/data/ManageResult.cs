﻿using System.Collections.Generic;

namespace FaceppSDK
{
    public struct ManageResult : IResult
    {
        private int added;
        private bool success;
        public int Added { get { return added; } set { added = value; } }
        public bool Success { get { return success; } set { success = value; } }


        public void CopyObject(Dictionary<string, object> obj)
        {
            added = int.Parse(obj["added"].ToString());
            success = bool.Parse(obj["success"].ToString());
        }
    }

    public struct ManageRemoveResult : IResult
    {

        private int removed;
        private bool success;
        public int Removed { get { return removed; } set { removed = value; } }
        public bool Success { get { return success; } set { success = value; } }



        public void CopyObject(Dictionary<string, object> obj)
        {
            removed = int.Parse(obj["removed"].ToString());
            success = bool.Parse(obj["success"].ToString());
        }
    }



    public struct ManageDeleteResult : IResult
    {

        private int deleted;
        private bool success;
        public int Deleted { get { return deleted; } set { deleted = value; } }
        public bool Success { get { return success; } set { success = value; } }


        public void CopyObject(Dictionary<string, object> obj)
        {
            deleted = int.Parse(obj["deleted"].ToString());
            success = bool.Parse(obj["success"].ToString());
        }
    }
}
