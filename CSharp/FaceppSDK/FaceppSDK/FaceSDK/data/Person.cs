﻿using System.Collections.Generic;

namespace FaceppSDK
{
    public struct FaceForPersonInfo
    {
        string face_id;
        string tag;
        public void CopyObject(Dictionary<string, object> obj)
        {
            face_id = obj["face_id"].ToString();
            tag = obj["tag"].ToString();
        }

    }


    public struct GroupBasicInfoNoNum
    {
        private string group_id;
        private string group_name;
        private string tag;

        public string Group_id { get { return group_id; } }
        public string Group_name { get { return group_name; } }
        public string Tag { get { return tag; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            group_id = obj["group_id"].ToString();
            group_name = obj["group_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }
    public struct PersonInfo:IResult
    {
         string person_id;
         string person_name;
         string tag;
         FaceForPersonInfo[] faces;
         GroupBasicInfoNoNum[] groups;


        public string Person_id { get { return person_id; } }
        public string Person_name { get { return person_name; } }
        public FaceForPersonInfo[]  Face_id { get { return faces; } }
        public string Tag { get { return tag; } }
        public GroupBasicInfoNoNum[] Groups { get { return groups; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            person_id = obj["persion_id"].ToString();
            person_name = obj["persion_name"].ToString();
            tag = obj["tag"].ToString();
            IList<object> arr = obj["face"] as List<object>;
            faces = new FaceForPersonInfo[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                FaceForPersonInfo face = new FaceForPersonInfo();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }

            arr = obj["group"] as List<object>;
            groups = new GroupBasicInfoNoNum[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                GroupBasicInfoNoNum group = new GroupBasicInfoNoNum();
                group.CopyObject(arr[i] as Dictionary<string, object>);
                groups[i] = group;
            }
        }
    }


    public struct PersonBasicInfo:IResult
    {
        private int added_face;
        private int added_group;
        private string person_id;
        private string person_name;
        private string tag;
        public int Added_face { get { return added_face; } }
        public int Added_group { get { return added_group; } }
        public string Person_id { get { return person_id; } }
        public string Person_name { get { return person_name; } }
        public string Tag { get { return tag; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            added_face = int.Parse(obj["added_face"].ToString());
            added_group = int.Parse(obj["added_group"].ToString());
            person_id = obj["person_id"].ToString();
            person_name = obj["person_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }
}
