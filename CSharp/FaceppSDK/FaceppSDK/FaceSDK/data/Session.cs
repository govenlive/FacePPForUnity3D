﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{

   

    public class TrainResult
    {
        string session_id;
        public string Session_id { get { return session_id; } }
        public void CopyObject(Dictionary<string, object> obj)
        {
            session_id = obj["session_id"].ToString();
        }
    }

   
    
   
    
   
   
   
    public class DetectSessionInfo:IResult
    {
        string status;
        DetectResult result;
        string session_id;
        public string Status { get { return status; } }
        public DetectResult Result { get { return result; } }
        public string Session_id { get { return session_id; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            status = obj["status"].ToString();
            session_id = obj["session_id"].ToString();
            result = new DetectResult();
            result.CopyObject(obj["result"] as Dictionary<string, object>);


        }
    }
    public class CompareSessionInfo:IResult
    {
        string status;
        CompareResult result;
        string session_id;
        public string Status { get { return status; } }
        public CompareResult Result { get { return result; } }
        public string Session_id { get { return session_id; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            status = obj["status"].ToString();
            session_id = obj["session_id"].ToString();
            result = new CompareResult();
            result.CopyObject(obj["result"] as Dictionary<string, object>);
        }
    }
    public class RecognizeSessionInfo:IResult
    {
        string status;
        IdentifyResult result;
        string session_id;

        public string Status { get { return status; } }
        public IdentifyResult Result { get { return result; } }
        public string Session_id { get { return session_id; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            status = obj["status"].ToString();
            session_id = obj["session_id"].ToString();
            result = new IdentifyResult();
            result.CopyObject(obj["result"] as Dictionary<string, object>);
        }



    }
    public class SearchSessionInfo:IResult
    {
        string status;
        SearchResult result;
        string session_id;

        public string Status { get { return status; } }
        public SearchResult Result { get { return result; } }
        public string Session_id { get { return session_id; } }


        public void CopyObject(Dictionary<string, object> obj)
        {
            status = obj["status"].ToString();
            session_id = obj["session_id"].ToString();
            result = new SearchResult();
            result.CopyObject(obj["result"] as Dictionary<string, object>);
        }
    }
    public class TrainSessionInfo:IResult
    {
        string status;
        TrainResult result;
        string session_id;

        public string Status { get { return status; } }
        public TrainResult Result { get { return result; } }
        public string Session_id { get { return session_id; } }


        public void CopyObject(Dictionary<string, object> obj)
        {
            status = obj["status"].ToString();
            session_id = obj["session_id"].ToString();
            result = new TrainResult();
            result.CopyObject(obj["result"] as Dictionary<string, object>);
        }
    }
    public class VerifySessionInfo:IResult
    {
        string status;
        VerifyResult result;
        string session_id;
        public string Status { get { return status; } }
        public VerifyResult Result { get { return result; } }
        public string Session_id { get { return session_id; } }


        public void CopyObject(Dictionary<string, object> obj)
        {
            status = obj["status"].ToString();
            session_id = obj["session_id"].ToString();
            result = new VerifyResult();
            result.CopyObject(obj["result"] as Dictionary<string, object>);
        }
    }
   

   
    public class FaceBasicInfo
    {
        string face_id;
        string tag;
        public string Face_id { get { return face_id; } }
        public string Tag { get { return tag; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            face_id = obj["face_id"].ToString();
            tag = obj["tag"].ToString();

        }
    }
    public class FaceSetInfoResult
    {
        public IList<FaceBasicInfo> face { get; set; }
        public string faceset_id { get; set; }
        public string faceset_name { get; set; }
        public string tag { get; set; }
    }
    public class FaceSetListInfo : IResult
    {
        FaceBasicInfo[] faceset;
        public FaceBasicInfo[] Faceset { get { return faceset; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            IList<object> arr = obj["faceset"] as List<object>;
            faceset = new FaceBasicInfo[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                FaceBasicInfo face = new FaceBasicInfo();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                faceset[i] = face;
            }
        }
    }

  
    public struct FaceGroupResultGroupList
    {
        FaceBasicInfo[] faces;
        public FaceBasicInfo[] Faces { get { return faces; } }
        public void CopyObject(IList<object> arr)
        {

            faces = new FaceBasicInfo[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                FaceBasicInfo face = new FaceBasicInfo();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }
        }
    }
    public class FaceGroupResultItem
    {
        FaceGroupResultGroupList[] grouplist;
        FaceBasicInfo[] ungrouped;
        public FaceGroupResultGroupList[] GroupList { get { return grouplist; } }
        public FaceBasicInfo[] Ungrouped { get { return ungrouped; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            IList<object> arr = obj["group"] as List<object>;
            grouplist = new FaceGroupResultGroupList[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                FaceGroupResultGroupList group = new FaceGroupResultGroupList();
                group.CopyObject(arr[i] as IList<object>);
                grouplist[i] = group;
            }

            arr = obj["ungrouped"] as List<object>;
            ungrouped = new FaceBasicInfo[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                FaceBasicInfo face = new FaceBasicInfo();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                ungrouped[i] = face;

            }

        }
    }
    public class GroupingResult:IResult
    {
        int create_time;
        int finish_time;
        string session_id;
        string status;
        FaceGroupResultItem result;
        public int Create_time { get { return create_time; } }
        public int Finish_time { get { return finish_time; } }
        public FaceGroupResultItem Result { get { return result; } }
        public string Session_id { get { return session_id; } }
        public string Status { get { return status; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            create_time = int.Parse(obj["create_time"].ToString());
            finish_time = int.Parse(obj["finish_time"].ToString());
            session_id = obj["session_id"].ToString();
            status = obj["status"].ToString();
            result = new FaceGroupResultItem();
            result.CopyObject(obj["result"] as Dictionary<string,object>);
        }
    }
  



}
