﻿using System.Collections.Generic;

namespace FaceppSDK
{
    /// <summary>
    /// 脸部属性
    /// </summary>
    public class Attribute
    {
        private Age age = new Age();
        private Gender gender = new Gender();
        private Glass glass = new Glass();
        private Pose pose = new Pose();
        private Race race = new Race();
        private Smiling smiling = new Smiling();
      
        /// <summary>
        /// 年龄
        /// </summary>
        public Age Age { get { return age; } }
        /// <summary>
        /// 性别
        /// </summary>
        public Gender Gender { get { return gender; } }
        /// <summary>
        /// 眼镜佩戴信息
        /// </summary>
        public Glass Glass { get { return glass; } }
        /// <summary>
        /// 脸部姿势
        /// </summary>
        public Pose Pose { get { return pose; } }

        /// <summary>
        /// 人种
        /// </summary>
        public Race Race { get { return race; } }
        /// <summary>
        /// 微笑程度
        /// </summary>
        public Smiling Smiling { get { return smiling; } }

        internal void copyObject(IDictionary<string,object> obj)
        {

            if (obj.Keys.Contains("age")) age.copyObject(obj["age"] as IDictionary<string, object>);
            if (obj.Keys.Contains("gender")) gender.copyObject(obj["gender"] as IDictionary<string, object>);
            if (obj.Keys.Contains("glass")) glass.copyObject(obj["glass"] as IDictionary<string, object>);
            if (obj.Keys.Contains("pose")) pose.copyObject(obj["pose"] as IDictionary<string, object>);
            if (obj.Keys.Contains("race")) race.copyObject(obj["race"] as IDictionary<string, object>);
            if (obj.Keys.Contains("smiling")) smiling.copyObject(obj["smiling"] as IDictionary<string, object>);
        }



    }
}
