﻿using System.Collections.Generic;




namespace FaceppSDK
{   

  
    /// <summary>
    /// 脸部信息
    /// </summary>
    public class Face 
    {
     
        Attribute attribute;
        Position position;
        string face_id;
        string tag;


        /// <summary>
        /// 被检测出的每一张人脸都在Face++系统中的标识符
        /// </summary>
        public string Face_id
        {
            get { return face_id; }
        }

        /// <summary>
        /// 分析的结果在数据库上的标签
        /// </summary>
        public string Tag
        {
            get { return tag; }
        }


        /// <summary>
        /// 脸部属性信息
        /// </summary>
        public Attribute Attribute
        {
            get { return attribute; }
        }
        /// <summary>
        /// 一些器官的位置信息
        /// </summary>
        public Position Position
        {
            get { return position; }
        }

        public  virtual void CopyObject(IDictionary<string, object> obj)
        {
            face_id = (string)obj["face_id"];
            tag = (string)obj["tag"];

            if (obj.Keys.Contains("attribute"))
            {
                attribute = new Attribute();
                attribute.copyObject(obj["attribute"] as IDictionary<string, object>);
            }
            if (obj.Keys.Contains("position"))
            {
                position = new Position();
                position.copyObject(obj["position"] as IDictionary<string, object>);
            }
        }


        public void CopyObjectFromGrouping(Dictionary<string, object> obj)
        {
            face_id = (string)obj["face_id"];
            tag = (string)obj["tag"];
        }
        public void CopyObjectFromDetection(IDictionary<string, object> obj)
        {
            face_id = (string)obj["face_id"];
            tag = (string)obj["tag"];

            attribute = new Attribute();
            attribute.copyObject(obj["attribute"] as IDictionary<string, object>);

            position = new Position();
            position.copyObject(obj["position"] as IDictionary<string, object>);
        }


        public void CopyObjectFromFacesetInfo(Dictionary<string, object> obj)
        {
            face_id = (string)obj["face_id"];
            tag = (string)obj["tag"];
        }


        private string url;
        private string img_id;
      

        public string Url
        {
            get { return url; }
        }

        public string Img_id
        {
            get { return img_id; }
        }

     

        public void CopyFromGet_face(IDictionary<string, object> obj)
        {
            attribute = new Attribute();
            attribute.copyObject(obj["attribute"] as Dictionary<string, object>);
            face_id = obj["face_id"].ToString();
            img_id = obj["img_id"].ToString();
            position = new Position();
            position.copyObject(obj["position"] as Dictionary<string, object>);

            tag = obj["tag"].ToString();
            url = obj["url"].ToString();
        }

      
    }
}
