﻿using System.Collections.Generic;

namespace FaceppSDK
{

    public enum GlassType
    {
        /// <summary>
        /// 未佩戴
        /// </summary>
        NONE,
        /// <summary>
        /// 黑超
        /// </summary>
        DARK,
        /// <summary>
        /// 普通眼镜
        /// </summary>
        NORMAL
    }
    /// <summary>
    /// 含眼镜佩戴分析结果
    /// </summary>
   public  struct Glass
    {

       private float _confidence;
       private GlassType value;

        /// <summary>
        /// 佩戴眼镜？ ( GlassType )
        /// </summary>
        public GlassType Value
        {
            get { return value; }
        }

        internal void copyObject(IDictionary<string,object> obj)
        {
            _confidence = float.Parse(obj["confidence"].ToString());
            if(_confidence>20)
            {
                if (obj["value"].ToString() == "Drak")
                {
                    value = GlassType.DARK;
                }
                else if(obj["value"].ToString()=="Normal")
                {
                    value = GlassType.NORMAL;
                }
                return;
            }
            value = GlassType.NONE;
        }
    }
}
