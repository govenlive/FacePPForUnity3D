﻿using System.Collections.Generic;

namespace FaceppSDK
{
    /// <summary>
    /// 器官位置
    /// </summary>
    public struct Point
    {

        private float x;
        private float y;


        /// <summary>
        /// 器官在脸部的百分比 X
        /// </summary>
        public float X
        {
            get { return x; }
        }

        /// <summary>
        /// 器官在脸部的百分比 Y
        /// </summary>
        public float Y
        {
            get { return y; }
        }
        /// <summary>
        /// 取值0-1
        /// </summary>
        public float X_p
        {
            get { return x * .01f; }
        }
        /// <summary>
        /// 取值0-1
        /// </summary>
        public float Y_p
        {
            get { return y * .01f; }
        }




        internal void copyObject(IDictionary<string, object> obj)
        {
            x = float.Parse(obj["x"].ToString());
            y = float.Parse(obj["y"].ToString());
        }


        public string ToString()
        {
            return "X-" + X.ToString() + "%   Y-" + Y.ToString() + "%";
        }
    }
}
