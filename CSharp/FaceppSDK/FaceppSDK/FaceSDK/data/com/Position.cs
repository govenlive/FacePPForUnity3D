﻿using System.Collections.Generic;

namespace FaceppSDK
{
    /// <summary>
    /// 脸部的大小和一些器官的位置
    /// </summary>
    public class Position
    {

        private float width = 0;
        private float height = 0;
        private Point center = new Point();
        private Point eye_left = new Point();
        private Point eye_right = new Point();
        private Point mouth_left = new Point();
        private Point mouth_right = new Point();
        private Point nose = new Point();


        public string ToString()
        {
            return "center:      " + center.ToString() + "\n" +
                   "eye_left:    " + eye_left.ToString() + "\n" +
                   "eye_right:   " + eye_right.ToString() + "\n" +
                   "mouth_left:  " + mouth_left.ToString() + "\n" +
                   "mouth_right: " + mouth_right.ToString() + "\n" +
                   "nose:        " + nose.ToString();
        }
        /// <summary>
        /// 脸部在图片里占的宽度百分比 (0-100)
        /// </summary>
        public float Width
        {
            get { return width; }
        }

        /// <summary>
        /// 脸部在图片里占的高度百分比 (0-100)
        /// </summary>
        public float Height
        {
            get { return height; }
        }

        /// <summary>
        /// 脸部中心点
        /// </summary>
        public Point Center
        {
            get { return center; }
        }

        /// <summary>
        /// 左眼
        /// </summary>
        public Point Eye_left
        {
            get { return eye_left; }
        }
        /// <summary>
        /// 右眼
        /// </summary>
        public Point Eye_right
        {
            get { return eye_right; }
        }
        /// <summary>
        /// 左嘴角
        /// </summary>
        public Point Mouth_left
        {
            get { return mouth_left; }
        }
        /// <summary>
        /// 右嘴角
        /// </summary>
        public Point Mouth_right
        {
            get { return mouth_right; }
        }
        /// <summary>
        /// 鼻子
        /// </summary>
        public Point Nose
        {
            get { return nose; }
        }

        internal void copyObject(IDictionary<string,object> obj)
        {


            width = float.Parse(obj["width"].ToString());
            height = float.Parse(obj["height"].ToString());

            center.copyObject(obj["center"] as IDictionary<string,object>);
            eye_left.copyObject(obj["eye_left"] as IDictionary<string,object>);
            eye_right.copyObject(obj["eye_right"] as IDictionary<string,object>);
            mouth_left.copyObject(obj["mouth_left"] as IDictionary<string,object>);
            mouth_right.copyObject(obj["mouth_right"] as IDictionary<string,object>);
            nose.copyObject(obj["nose"] as IDictionary<string,object>);


        }

    }
}
