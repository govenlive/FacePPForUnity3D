﻿using System.Collections.Generic;

namespace FaceppSDK
{
    /// <summary>
    ///  包含微笑程度分析结果，value的值为0－100的实数，越大表示微笑程度越高
    /// </summary>
    public struct Smiling
    {
        private float value;

        /// <summary>
        /// 微笑指数 0-100
        /// </summary>
        public float Value
        {
            get { return value; }
        }

        internal void copyObject(IDictionary<string,object> obj)
        {
            value = float.Parse(obj["value"].ToString());
        }
    }
}
