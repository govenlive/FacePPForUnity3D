﻿using System.Collections.Generic;

namespace FaceppSDK
{
    public class AppInfo : IResult
    {
        private string description;
        private string name;
        public string Description { get { return description; } }
        public string Name { get { return name; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            description = obj["description"].ToString();
            name = obj["name"].ToString();
        }
    }
}
