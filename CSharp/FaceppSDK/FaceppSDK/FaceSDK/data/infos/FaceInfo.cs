﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{
    public struct FaceInfoItem
    {
        string person_name;
        string url;
        string img_id;
        string tag;
        string person_id;
        string face_id;
        Position position;

        public string Person_name { get { return person_name; } }
        public string Url { get { return url; } }
        public string Img_id { get { return img_id; } }
        public string Tag { get { return tag; } }
        public string Person_id { get { return person_id; } }
        public Position Position { get { return position; } }
        public string Face_id { get { return face_id; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            person_name = obj["person_name"].ToString();
            if (obj["url"] != null)
            {
                url = obj["url"].ToString();
            }
            else
            {
                url = "";
            }
            img_id = obj["img_id"].ToString();
            tag = obj["tag"].ToString();
            person_id = obj["person_id"].ToString();
            face_id = obj["face_id"].ToString();
            position = new Position();
            position.copyObject(obj["position"] as Dictionary<string, object>);

        }
    }
    public class FaceInfoList:IResult
    {
        private FaceInfoItem[] face_infos;
        public FaceInfoItem[] Face_infos { get { return face_infos; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            IList<object> arr = obj["face_info"] as List<object>;
            face_infos = new FaceInfoItem[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                FaceInfoItem face_info = new FaceInfoItem();
                face_info.CopyObject(arr[i] as Dictionary<string, object>);
                face_infos[i] = face_info;
            }
        }
    }
}
