﻿using System.Collections.Generic;

namespace FaceppSDK
{
    public class GroupInfoList : IResult
    {
        GroupBasicInfo[] groups;

        public GroupBasicInfo[] Groups { get { return groups; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            IList<object> arr = obj["group"] as List<object>;
            groups = new GroupBasicInfo[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                GroupBasicInfo group = new GroupBasicInfo();
                group.CopyObject(arr[i] as Dictionary<string, object>);
                groups[i] = group;
            }
        }
    }
}
