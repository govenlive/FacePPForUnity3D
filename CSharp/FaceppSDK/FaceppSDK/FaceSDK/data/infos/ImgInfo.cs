﻿using System.Collections.Generic;

namespace FaceppSDK
{
    public class FaceItemOfImgInfo
    {
        Position position;
        string face_id;
        string tag;

        public Position Position { get { return position; } }
        public string Face_id { get { return face_id; } }
        public string Tag { get { return tag; } }
        public void CopyObject(Dictionary<string, object> obj)
        {
            position = new Position();
            position.copyObject(obj["position"] as Dictionary<string, object>);
            face_id = obj["face_id"].ToString();
            tag = obj["tag"].ToString();

        }
    }
    public class ImgInfo : IResult
    {
        string url;
        string img_id;
        FaceItemOfImgInfo[] faces;
        public string Url { get { return url; } }
        public string Img_id { get { return img_id; } }
        public FaceItemOfImgInfo[] Faces { get { return faces; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            if (obj["url"] != null)
            {
                url = obj["url"].ToString();
            }
            else
            {
                url = "";
            }
            img_id = obj["img_id"].ToString();
            IList<object> arr = obj["face"] as List<object>;
            faces = new FaceItemOfImgInfo[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                FaceItemOfImgInfo face = new FaceItemOfImgInfo();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }

        }
    }
}
