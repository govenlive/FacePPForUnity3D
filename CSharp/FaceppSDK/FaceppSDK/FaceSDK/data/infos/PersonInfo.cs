﻿using System.Collections.Generic;

namespace FaceppSDK
{
    public class PersonInfoList : IResult
    {
        PersonBasicInfo[] persons;
        public PersonBasicInfo[] Persons { get { return persons; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            IList<object> arr = obj["person"] as List<object>;
            persons = new PersonBasicInfo[arr.Count];
            for (int i = 0; i < arr.Count; i++)
            {
                PersonBasicInfo person = new PersonBasicInfo();
                person.CopyObject(arr[i] as Dictionary<string, object>);
                persons[i] = person;
            }
        }
    }
}
