﻿using System.Collections.Generic;

namespace FaceppSDK
{
    public class QuotaInfo : IResult
    {
        private int quota_all;
        private int quota_search;

        public int QUOTA_ALL { get { return quota_all; } }
        public int QUOTA_SEARCH { get { return quota_search; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            quota_all = int.Parse(obj["quota_all"].ToString());
            quota_search = int.Parse(obj["quota_search"].ToString());
        }
    }
}
