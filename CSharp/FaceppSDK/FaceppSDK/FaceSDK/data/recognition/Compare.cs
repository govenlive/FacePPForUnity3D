﻿using System.Collections.Generic;

namespace FaceppSDK
{
    public struct CompareResult:IResult
    {
        private Component_Similarity component_similarity;
        private float similarity;
        private string session_id;
       
        public Component_Similarity Component_similarity { get { return component_similarity; } }
        public string Session_id { get { return session_id; } }
        public float Similarity { get{return similarity;} }

        public void CopyObject(Dictionary<string, object> obj)
        {
            component_similarity = new Component_Similarity();
            component_similarity.CopyObject(obj["component_similarity"] as Dictionary<string, object>);
            session_id = obj["session_id"].ToString();
            similarity = float.Parse(obj["similarity"].ToString());
        }
    }


    /// <summary>
    /// 包含人脸中各个部位的相似性，目前包含eyebrow(眉毛)、eye(眼睛)、nose(鼻子)与mouth（嘴）的相似性。每一项的值为一个0~100之间的实数，表示相应部位的相似性
    /// </summary>
    public struct Component_Similarity
    {
        private float mouth;
        private float eye;
        private float nose;
        private float eyebrow;
        public double Mouth { get { return mouth; } }
        public double Eye { get{return eye;} }
        public double Nose { get{return nose;}}
        public double Eyebrow { get { return eyebrow; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            mouth = float.Parse(obj["mouth"].ToString());
            eye = float.Parse(obj["eye"].ToString());
            nose = float.Parse(obj["nose"].ToString());
            eyebrow = float.Parse(obj["eyebrow"].ToString());
        }
    }
}
