﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{
    public struct CandidateItemOfRecognize
    {
        private float confidence;
        private string person_id;
        private string person_name;
        private string tag;
        public double Confidence { get { return confidence; } }
        public string Person_id { get { return person_id; } }
        public string Person_name { get { return person_name; } }
        public string Tag { get { return tag; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            confidence = float.Parse(obj["confidence"].ToString());
            person_id = obj["person_id"].ToString();
            person_name = obj["person_name"].ToString();
            tag = obj["tag"].ToString();
        }
    }

    public struct FaceItemOfRecognize
    {
        private Position position;
        private string face_id;
        private CandidateItemOfRecognize[] candidates;

        public Position Position { get { return position; } }
        public string Face_id { get { return face_id; } }
        public CandidateItemOfRecognize[] Candidates { get { return candidates; } }
        public void CopyObject(Dictionary<string, object> obj)
        {
            position = new Position();
            position.copyObject(obj["position"] as Dictionary<string, object>);
            face_id = obj["face_id"].ToString();
            IList<object> arr = obj["candidate"] as List<object>;
            candidates = new CandidateItemOfRecognize[arr.Count];
            for(int i=0;i<arr.Count;i++)
            {
                CandidateItemOfRecognize candidate = new CandidateItemOfRecognize();
                candidate.CopyObject(arr[i] as Dictionary<string, object>);
                candidates[i] = candidate;
            }
        }
    }
    /// <summary>
    /// 对于一个待查询的Face列表（或者对于给定的Image中所有的Face），在一个Group中查询最相似的Person。
    /// </summary>
    public struct IdentifyResult:IResult
    {
        private string session_id;
        private FaceItemOfRecognize[] faces;

        public string Session_id { get { return session_id; } }
        public FaceItemOfRecognize[] Faces { get { return faces; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            session_id = obj["session_id"].ToString();
            IList<object> arr = obj["face"] as List<object>;
            faces = new FaceItemOfRecognize[arr.Count];
            for(int i=0;i<arr.Count;i++)
            {
                FaceItemOfRecognize face = new FaceItemOfRecognize();
                face.CopyObject(arr[i] as Dictionary<string, object>);
                faces[i] = face;
            }
        }
    }

}
