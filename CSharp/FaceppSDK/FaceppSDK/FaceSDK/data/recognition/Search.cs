﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{
    public struct CandidateItemOfSearch
    {
        private string face_id;
        private string similarity;
        private string tag;


        public string Face_id { get { return face_id; } }
        public string Similarity { get { return similarity; } }
        public string Tag { get { return tag; } }
        public void CopyObject(Dictionary<string, object> obj)
        {
            face_id = obj["face_id"].ToString();
            similarity = obj["similarity"].ToString();
            tag = obj["tag"].ToString();
        }
    }
    public struct SearchResult:IResult
    {
        private CandidateItemOfSearch[] candidates;
        private string session_id;
        public CandidateItemOfSearch[] Candidates { get { return candidates; } }
        public string Session_id { get { return session_id; } }
        public void CopyObject(Dictionary<string, object> obj)
        {
            session_id = obj["session_id"].ToString();
            IList<object> arr = obj["candidate"] as List<object>;
            candidates = new CandidateItemOfSearch[arr.Count];
            for(int i=0;i<arr.Count;i++)
            {
                CandidateItemOfSearch candidate = new CandidateItemOfSearch();
                candidate.CopyObject(arr[i] as Dictionary<string, object>);
                candidates[i] = candidate;
            }
        }
    }
}
