﻿using System.Collections.Generic;

namespace FaceppSDK
{
    /// <summary>
    /// 给定一个Face和一个Person，返回是否是同一个人的判断以及置信度。
    /// </summary>
    public struct VerifyResult:IResult
    {
        private float confidence;
        private bool is_same_person;
        private string session_id;
        public float Confidence { get { return confidence; } }
        public bool Is_same_person { get { return is_same_person; } }
        public string Session_id { get { return session_id; } }

        public void CopyObject(Dictionary<string, object> obj)
        {
            confidence = float.Parse(obj["confidence"].ToString());
            is_same_person = bool.Parse(obj["is_same_person"].ToString());
            session_id = obj["session_id"].ToString();
        }
    }
}
