﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using FacePP.Detection;

using FaceppSDK;

public class PhotoAndGetFaceDetect : MonoBehaviour {
    public string Mydevice;
    public RawImage CamImg;
    public Text TextInput;
    public InputField field_key;
    public InputField field_secret;

    WebCamTexture tex;

    private FaceService  faceService;
	// Use this for initialization
    IEnumerator Start()
    {
        yield return Application.RequestUserAuthorization(UserAuthorization.WebCam);
        if (Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            WebCamDevice[] devices = WebCamTexture.devices;
            if (devices.Length > 0)
            {
                Mydevice = devices[0].name;
                tex = new WebCamTexture(Mydevice, 1920, 1080, 15);
                CamImg.texture = tex;
                tex.Play();
            }
           
        }
        faceService = new FaceService("967fecd83e2bb359274edaf11a867779", "aBK8zRI7dJJpH1pJ7eBHDNgeOBhlTKZI");
     //  FacePP.Facepp_config.api_key = "967fecd83e2bb359274edaf11a867779";
      // FacePP.Facepp_config.api_secret = "aBK8zRI7dJJpH1pJ7eBHDNgeOBhlTKZI";
     //  field_key.text = FacePP.Facepp_config.api_key;
      // field_secret.text = FacePP.Facepp_config.api_secret;
      // field_key.text = constan
     //  field_secret.text = FacePP.Facepp_config.api_secret;
    }

    public void changeKey(){
      //  FacePP.Facepp_config.api_key = field_key.text;
    
    }
    public void changeSecret()
    {
       // FacePP.Facepp_config.api_secret = field_secret.text;
    }
    public void Photo()
    {
        StartCoroutine(photoAnd2faceServer());
    }
    IEnumerator photoAnd2faceServer()
    {
        TextInput.text = "";
        int width = Screen.width;
        int height = Screen.height;
        yield return new WaitForEndOfFrame(); //去掉协程试试看会发生什么。
        Texture2D tex = new Texture2D(width, height, TextureFormat.RGB24, false);//设置Texture2D
        tex.ReadPixels(new Rect(0, 0, width, height), 0, 0);//获取Pixels           
        tex.Apply();//应用改变
        byte[] bytes = tex.EncodeToJPG(5);//转换为Jpeg byte[],且质量降低些，这个byte是用来上传服务器分析人脸的，所以越小越好.
        Destroy(tex);

        TextInput.text = "开始检测.\n";

         DetectResult result = faceService.Detection_DetectImg(bytes);
         Debug.Log(result);
      //  ISession session = Facepp_detect.instance.DetectSession(bytes);
   //     Debug.Log(Facepp_detect.instance.JsonStr);

      //  Debug.Log(result.Faces.Length);


        try
        {
          
            //Debug.Log(session.Img_id);
            TextInput.text += "<color=brown><size=15>Img_id:     </size></color>" + result.Img_id + "\n";
            TextInput.text += "<color=brown><size=15>Session_id: </size></color>" + result.Session_id + "\n";
            TextInput.text += "<color=brown><size=15>FaceCout:   </size></color>" + result.Faces.Length.ToString() + "\n";


            for (int i = 0; i < result.Faces.Length; i++)
            {
               // IFace face = session.Faces[i];

              //  Face face = result.Faces[i];


               // TextInput.text += ".................................... \n";
               // TextInput.text += "<color=brown><size=15>FaceIndex: </size></color> " + (i + 1).ToString() + " \n";
               // TextInput.text += "<color=brown><size=15>Face_id:   </size></color> " + face.Face_id + "\n";
               // TextInput.text += "<color=brown><size=15>Tag:       </size></color> " + face.Tag + "\n";
               // TextInput.text += "<color=brown><size=15>Postion:   </size></color> \n" + face.Position.ToString() + "\n";
               // TextInput.text += "<color=brown><size=15>Age:       </size></color>" + face.Attribute.Age.ToString() + "\n";
               // TextInput.text += "<color=brown><size=15>Gender:    </size></color> " + face.Attribute.Gender.Value.ToString() + "\n";
               // TextInput.text += "<color=brown><size=15>Smiling:   </size></color> " + face.Attribute.Smiling.Value.ToString() + "\n";
               // TextInput.text += "<color=brown><size=15>Race:      </size></color> " + face.Attribute.Race.Value + "\n";
               //// TextInput.text += "<color=brown><size=15>Glass:     </size></color> " + face.Attribute.Glass.Value + "\n";
               // TextInput.text += "<color=brown><size=15>Pose:      </size></color> " + face.Attribute.Pose.ToString() + "\n";
            }
        }
        catch (System.Exception ex)
        {
            TextInput.text += ex.Message;
           // throw;
        }      
    }
}
